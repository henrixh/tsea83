-- |In this module we define all datatypes needed for our assembly
-- language, as well as helper functions to help us wrap it up later
-- on. We also specialize the Asm monad to our language and call it
-- TAsm.

module TAsmMonad where
import Data.Word
import MonadAsm
import Numeric
import Data.Char


-- |We use 16-bit addresses
type Position = Word16
-- |Datatype for an instruction
data Instruction = Instruction {
      operand :: Operand
    , amod :: Mode
    , gra :: Reg
    , grb :: Reg
    }

instance Show Instruction where
         show (Instruction operand' amod' gra' grb') = unwords
                                                       [ show operand'
                                                       , show amod'
                                                       , show gra'
                                                       , show grb'
                                                       ]

 -- Some functions for easy printing
prettyShowRows :: [Row] -> String
prettyShowRows rs = unlines $ zipWith (++)
                (map ((++ ": ") . show) [0,2..])
                (map show rs)

showRowBinary :: Row -> String
showRowBinary (DataR w) = showBin 16 $ fromIntegral w
showRowBinary (InstrR (Instruction o am a b)) =
  concat [ binFromEnum 6 o
         , binFromEnum 2 am
         , binFromEnum 4 a
         , binFromEnum 4 b
         ]

binFromEnum :: (Enum a) => Int -> a -> String
binFromEnum len d = padTo len $ showIntAtBase 2 intToDigit (fromEnum d) ""

showBin :: Int -> Int -> String
showBin len d = padTo len $ showIntAtBase 2 intToDigit (fromEnum d) ""

padTo :: Int -> String -> String
padTo i xs = if length xs == i
             then xs
             else padTo i $ '0':xs

-- |Define a Row to be either data in the form of a Word16, or an
-- Instruction, as defined above. Later on we need to translate this
-- to raw binary.
data Row = DataR Word16
         | InstrR Instruction

instance Show Row where
  show (DataR d) = show d
  show (InstrR i) = show i

-- |Adressing modes.
data Mode = Dir
          | Im
          | In
          | Ix
            deriving (Show, Enum)

-- |Datatype for the registers
data Reg = GR0
         | GR1
         | GR2
         | GR3
         | GR4
         | GR5
         | GR6
         | GR7
         | GR8
         | GR9
         | GR10
         | GR11
         | GR12
         | GR13
         | GR14
         | GR15
           deriving (Enum, Show, Eq) -- Enumerate the registers for ease
                                     -- of use later on

-- |Datatype for the operands.
data Operand = NOP
             | HALT
             | GLOB
             | JMP
             | JNZ
             | JZ
             | JEQ
             | JNE
             | JSR
             | RTS
             | RTI
             | SFLAG
             | LFLAG
             | PUSH
             | POP
             | SSP
             | LSP
             | LOAD
             | STORE
             | ADD
             | SUB
             | MUL
             | MULU
             | LSL
             | LSR
             | ASR
             | ASL
             | LRR
             | LRL
             | AND
             | OR
             | NOT
             | XOR
             | CP
             | SWAP
             | CLR
               deriving (Enum, Show) -- Enumerate and make an instance
                                     -- of Show.

-- |Datatype for Global registers.
data Global = Glob0
            | Glob1
            | Glob2
            | Glob3
            | Glob4
            | Glob5
            | Glob6
            | Glob7
            | Glob8
            | Glob9
            | Glob10
            | Glob11
            | Glob12
            | Glob13
            | Glob14
            | Glob15
            deriving (Enum, Show)

-- |The global enum should be stored in the space a register would
-- otherwise have used. Need to convert. Oh, and here be dragons if
-- len(global) > len(reg)
globToReg :: Global -> Reg
globToReg = toEnum . fromEnum

-- |Specialize the monadic state to our purposes.
type TAsmState = AsmState Row Position
-- |Create a type synonym
type TAsm = Asm Row Position

-- |Run the monad
assemble :: TAsm a -> [Row]
assemble (Asm f) = d
    where
      (_,(d,_)) = f initialState
      initialState = ([], 0x0000)

-- |Define a master instruction, from which all the others are
-- built. It simply does nothing.
nopInstr :: Instruction
nopInstr = Instruction {
           operand = NOP
         , amod = Dir
         , gra = GR0
         , grb = GR0
         }

-- Define some helper functions to help abstract away the underlying
-- monad later on

-- |Add a row to the monadic state.
row :: Row -> TAsm ()
row r = Asm $ \(rows, pos) -> ((), (rows ++ [r], pos + 1))

-- |Give the current address. No change to state.
label :: TAsm Position
-- Note the (*2) because we are addressing on bytes, not words.
label = Asm $ \(d,r) -> (r * 2,(d,r))

-- |This function enables us to set the adressing mode after the row
-- was created. This should be a rather slow method, but enables us to
-- use some nice syntax in the EDSL.
setAMod :: Mode -> TAsm ()
setAMod m = Asm $ \(rows, pos) -> ((),
                                      (
                                       changeNthElement
                                        (pos - 2)
                                        (\(InstrR x) -> InstrR $ x { amod = m })
                                        rows
                                      , pos)
                                 )
    where

        -- <sarcasm> It is a very good idea to use this function below if you
        -- have demands on execution speed <\sarcasm>
        changeNthElement :: Position -> (a -> a) -> [a] -> [a]
        changeNthElement idx transform list
            | idx < 0   = list
            | otherwise = case splitAt (fromIntegral idx) list of
                            (front, element:back) -> front ++ transform element
                                                    : back
                            _ -> list    -- if the list doesn't have an
                                        -- element at index idx
