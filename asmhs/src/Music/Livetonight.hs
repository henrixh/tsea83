{-# LANGUAGE RecursiveDo #-}
-- |Main livetonight module
module Music.Livetonight where
import TAsm
 
-- |Livetonight data
livetonight :: TAsm ()
livetonight = do
  dataRow 0x732C
  dataRow 0x3844
  dataRow 0x7300
  dataRow 0x3B47
  dataRow 0x732C
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x384B
  dataRow 0x732C
  dataRow 0x3B42
  dataRow 0x7300
  dataRow 0x3F44
  dataRow 0x732F
  dataRow 0x3B47
  dataRow 0x732F
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x4253
  dataRow 0x732F
  dataRow 0x3B52
  dataRow 0x732F
  dataRow 0x3F4E
  dataRow 0x7300
  dataRow 0x424B
  dataRow 0x732F
  dataRow 0x3B47
  dataRow 0x732F
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x424B
  dataRow 0x732F
  dataRow 0x3B4E
  dataRow 0x732F
  dataRow 0x3F44
  dataRow 0x7300
  dataRow 0x4247
  dataRow 0x732F
  dataRow 0x3B49
  dataRow 0x732F
  dataRow 0x3F4B
  dataRow 0x7300
  dataRow 0x4242
  dataRow 0x732F
  dataRow 0x3B44
  dataRow 0x732F
  dataRow 0x3F47
  dataRow 0x7300
  dataRow 0x4249
  dataRow 0x7333
  dataRow 0x3F53
  dataRow 0x7300
  dataRow 0x4252
  dataRow 0x7333
  dataRow 0x464E
  dataRow 0x7300
  dataRow 0x3F50
  dataRow 0x7333
  dataRow 0x4247
  dataRow 0x7300
  dataRow 0x4649
  dataRow 0x7333
  dataRow 0x3F4B
  dataRow 0x7300
  dataRow 0x424E
  dataRow 0x732E
  dataRow 0x3A44
  dataRow 0x732E
  dataRow 0x3D47
  dataRow 0x7300
  dataRow 0x4149
  dataRow 0x732E
  dataRow 0x3A4B
  dataRow 0x732E
  dataRow 0x3D42
  dataRow 0x7300
  dataRow 0x4144
  dataRow 0x732F
  dataRow 0x3B47
  dataRow 0x732F
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x4253
  dataRow 0x732F
  dataRow 0x3B52
  dataRow 0x732F
  dataRow 0x3F4E
  dataRow 0x7300
  dataRow 0x424B
  dataRow 0x732F
  dataRow 0x3B47
  dataRow 0x732F
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x424B
  dataRow 0x732F
  dataRow 0x3B4E
  dataRow 0x732F
  dataRow 0x3F44
  dataRow 0x7300
  dataRow 0x4247
  dataRow 0x732F
  dataRow 0x3B49
  dataRow 0x732F
  dataRow 0x3F4B
  dataRow 0x7300
  dataRow 0x4242
  dataRow 0x732F
  dataRow 0x3B44
  dataRow 0x732F
  dataRow 0x3F47
  dataRow 0x7300
  dataRow 0x4249
  dataRow 0x732F
  dataRow 0x3B53
  dataRow 0x7300
  dataRow 0x3F52
  dataRow 0x732F
  dataRow 0x424E
  dataRow 0x7300
  dataRow 0x3B50
  dataRow 0x732F
  dataRow 0x3F47
  dataRow 0x7300
  dataRow 0x4249
  dataRow 0x732A
  dataRow 0x3B4B
  dataRow 0x7300
  dataRow 0x3F4E
  dataRow 0x732C
  dataRow 0x3844
  dataRow 0x7300
  dataRow 0x3B47
  dataRow 0x732C
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x384B
  dataRow 0x732C
  dataRow 0x3B42
  dataRow 0x7300
  dataRow 0x3F44
  dataRow 0x732F
  dataRow 0x3B47
  dataRow 0x732F
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x4253
  dataRow 0x732F
  dataRow 0x3B52
  dataRow 0x732F
  dataRow 0x3F4E
  dataRow 0x7300
  dataRow 0x424B
  dataRow 0x732F
  dataRow 0x3B47
  dataRow 0x732F
  dataRow 0x3F49
  dataRow 0x7300
  dataRow 0x424B
  dataRow 0x732F
  dataRow 0x3B4E
  dataRow 0x732F
  dataRow 0x3F44
  dataRow 0x7300
  dataRow 0x4247
  dataRow 0x732F
  dataRow 0x3B49
  dataRow 0x732F
  dataRow 0x3F4B
  dataRow 0x7300
  dataRow 0x4242
  dataRow 0x732F
  dataRow 0x3B44
  dataRow 0x732F
  dataRow 0x3F47
  dataRow 0x7300
  dataRow 0x4249
  dataRow 0x732A
  dataRow 0x3653
  dataRow 0x7300
  dataRow 0x3A52
  dataRow 0x732A
  dataRow 0x3D4E
  dataRow 0x7300
  dataRow 0x3650
  dataRow 0x732A
  dataRow 0x3A47
  dataRow 0x7300
  dataRow 0x3D49
  dataRow 0x732A
  dataRow 0x364B
  dataRow 0x7300
  dataRow 0x3A4E
  dataRow 0x7327
  dataRow 0x3344
  dataRow 0x7327
  dataRow 0x3647
  dataRow 0x7300
  dataRow 0x3A49
  dataRow 0x7327
  dataRow 0x334B
  dataRow 0x7327
  dataRow 0x3642
  dataRow 0x7300
  dataRow 0x3A44
  dataRow 0x732C
  dataRow 0x3847
  dataRow 0x732C
  dataRow 0x3B49
  dataRow 0x7300
  dataRow 0x3F53
  dataRow 0x732C
  dataRow 0x3852
  dataRow 0x732C
  dataRow 0x3B4E
  dataRow 0x7300
  dataRow 0x3F4B
  dataRow 0x732C
  dataRow 0x3847
  dataRow 0x732C
  dataRow 0x3B49
  dataRow 0x7300
  dataRow 0x3F4B
  dataRow 0x732C
  dataRow 0x384E
  dataRow 0x732C
  dataRow 0x3B44
  dataRow 0x7300
  dataRow 0x3F47
  dataRow 0x732C
  dataRow 0x3849
  dataRow 0x732C
  dataRow 0x3B4B
  dataRow 0x7300
  dataRow 0x3F42
  dataRow 0x732C
  dataRow 0x3844
  dataRow 0x732C
  dataRow 0x3B47
  dataRow 0x7300
  dataRow 0x3F49
  dataRow 0x732C
  dataRow 0x3853
  dataRow 0x7300
  dataRow 0x3B52
  dataRow 0x732C
  dataRow 0x3F4E
  dataRow 0x7300
  dataRow 0x3850
  dataRow 0x732C
  dataRow 0x3B47
  dataRow 0x7300
  dataRow 0x3F49
  dataRow 0x732C
  dataRow 0x384B
  dataRow 0x7300
  dataRow 0x3B4E
  dataRow 0x0000
  dataRow 0x0000
