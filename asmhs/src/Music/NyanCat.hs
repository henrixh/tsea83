{-# LANGUAGE RecursiveDo #-}
-- |Main nyanCat module
module Music.NyanCat where
import TAsm
 
-- |NyanCat data
nyanCat :: TAsm ()
nyanCat = do
  dataRow 0x694F
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x6951
  dataRow 0x3541
  dataRow 0x6900
  dataRow 0x3541
  dataRow 0x694B
  dataRow 0x3700
  dataRow 0x694C
  dataRow 0x3700
  dataRow 0x6900
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3743
  dataRow 0x694B
  dataRow 0x3400
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3440
  dataRow 0x6900
  dataRow 0x3440
  dataRow 0x6948
  dataRow 0x3900
  dataRow 0x6900
  dataRow 0x3900
  dataRow 0x694A
  dataRow 0x3945
  dataRow 0x6900
  dataRow 0x3945
  dataRow 0x694B
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x694B
  dataRow 0x323E
  dataRow 0x694A
  dataRow 0x323E
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x694A
  dataRow 0x3700
  dataRow 0x694C
  dataRow 0x3743
  dataRow 0x694F
  dataRow 0x3743
  dataRow 0x6951
  dataRow 0x3000
  dataRow 0x694C
  dataRow 0x3000
  dataRow 0x694F
  dataRow 0x303C
  dataRow 0x694A
  dataRow 0x303C
  dataRow 0x694C
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694C
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x694F
  dataRow 0x3541
  dataRow 0x6900
  dataRow 0x3541
  dataRow 0x6951
  dataRow 0x3700
  dataRow 0x694C
  dataRow 0x3700
  dataRow 0x694F
  dataRow 0x3743
  dataRow 0x694A
  dataRow 0x3743
  dataRow 0x694C
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694B
  dataRow 0x3440
  dataRow 0x694C
  dataRow 0x3440
  dataRow 0x694B
  dataRow 0x3900
  dataRow 0x694A
  dataRow 0x3900
  dataRow 0x6948
  dataRow 0x3945
  dataRow 0x694A
  dataRow 0x3945
  dataRow 0x694B
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x323E
  dataRow 0x694A
  dataRow 0x323E
  dataRow 0x694C
  dataRow 0x3700
  dataRow 0x694F
  dataRow 0x3700
  dataRow 0x694A
  dataRow 0x3743
  dataRow 0x694B
  dataRow 0x3743
  dataRow 0x694A
  dataRow 0x3000
  dataRow 0x6948
  dataRow 0x3000
  dataRow 0x694A
  dataRow 0x303C
  dataRow 0x6900
  dataRow 0x303C
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x6900
  dataRow 0x3400
  dataRow 0x694F
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x6951
  dataRow 0x3541
  dataRow 0x6900
  dataRow 0x3541
  dataRow 0x694B
  dataRow 0x3700
  dataRow 0x694C
  dataRow 0x3700
  dataRow 0x6900
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3743
  dataRow 0x694B
  dataRow 0x3400
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3440
  dataRow 0x6900
  dataRow 0x3440
  dataRow 0x6948
  dataRow 0x3900
  dataRow 0x6900
  dataRow 0x3900
  dataRow 0x694A
  dataRow 0x3945
  dataRow 0x6900
  dataRow 0x3945
  dataRow 0x694B
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x694B
  dataRow 0x323E
  dataRow 0x694A
  dataRow 0x323E
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x694A
  dataRow 0x3700
  dataRow 0x694C
  dataRow 0x3743
  dataRow 0x694F
  dataRow 0x3743
  dataRow 0x6951
  dataRow 0x3000
  dataRow 0x694C
  dataRow 0x3000
  dataRow 0x694F
  dataRow 0x303C
  dataRow 0x694A
  dataRow 0x303C
  dataRow 0x694C
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694C
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x694F
  dataRow 0x3541
  dataRow 0x6900
  dataRow 0x3541
  dataRow 0x6951
  dataRow 0x3700
  dataRow 0x694C
  dataRow 0x3700
  dataRow 0x694F
  dataRow 0x3743
  dataRow 0x694A
  dataRow 0x3743
  dataRow 0x694C
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694B
  dataRow 0x3440
  dataRow 0x694C
  dataRow 0x3440
  dataRow 0x694B
  dataRow 0x3900
  dataRow 0x694A
  dataRow 0x3900
  dataRow 0x6948
  dataRow 0x3945
  dataRow 0x694A
  dataRow 0x3945
  dataRow 0x694B
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x323E
  dataRow 0x694A
  dataRow 0x323E
  dataRow 0x694C
  dataRow 0x3700
  dataRow 0x694F
  dataRow 0x3700
  dataRow 0x694A
  dataRow 0x3743
  dataRow 0x694B
  dataRow 0x3743
  dataRow 0x694A
  dataRow 0x3000
  dataRow 0x6948
  dataRow 0x3000
  dataRow 0x694A
  dataRow 0x303C
  dataRow 0x6900
  dataRow 0x303C
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x6900
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x6943
  dataRow 0x3541
  dataRow 0x6945
  dataRow 0x3541
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x6900
  dataRow 0x3700
  dataRow 0x6943
  dataRow 0x3743
  dataRow 0x6945
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x694C
  dataRow 0x3440
  dataRow 0x6948
  dataRow 0x3440
  dataRow 0x694D
  dataRow 0x3900
  dataRow 0x694C
  dataRow 0x3900
  dataRow 0x694D
  dataRow 0x3945
  dataRow 0x694F
  dataRow 0x3945
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x323E
  dataRow 0x6900
  dataRow 0x323E
  dataRow 0x6943
  dataRow 0x3700
  dataRow 0x6945
  dataRow 0x3700
  dataRow 0x6948
  dataRow 0x3743
  dataRow 0x6943
  dataRow 0x3743
  dataRow 0x694D
  dataRow 0x3000
  dataRow 0x694C
  dataRow 0x3000
  dataRow 0x694A
  dataRow 0x303C
  dataRow 0x6948
  dataRow 0x303C
  dataRow 0x6943
  dataRow 0x3200
  dataRow 0x6942
  dataRow 0x3200
  dataRow 0x6941
  dataRow 0x3400
  dataRow 0x6943
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x6943
  dataRow 0x3541
  dataRow 0x6945
  dataRow 0x3541
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x6900
  dataRow 0x3700
  dataRow 0x6943
  dataRow 0x3743
  dataRow 0x6945
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694A
  dataRow 0x3440
  dataRow 0x694C
  dataRow 0x3440
  dataRow 0x6948
  dataRow 0x3900
  dataRow 0x6943
  dataRow 0x3900
  dataRow 0x6945
  dataRow 0x3945
  dataRow 0x6943
  dataRow 0x3945
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x323E
  dataRow 0x6947
  dataRow 0x323E
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x6943
  dataRow 0x3700
  dataRow 0x6945
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3743
  dataRow 0x694D
  dataRow 0x3000
  dataRow 0x694C
  dataRow 0x3000
  dataRow 0x694D
  dataRow 0x303C
  dataRow 0x694F
  dataRow 0x303C
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6947
  dataRow 0x3400
  dataRow 0x6900
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x6943
  dataRow 0x3541
  dataRow 0x6945
  dataRow 0x3541
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x6900
  dataRow 0x3700
  dataRow 0x6943
  dataRow 0x3743
  dataRow 0x6945
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x694C
  dataRow 0x3440
  dataRow 0x6948
  dataRow 0x3440
  dataRow 0x694D
  dataRow 0x3900
  dataRow 0x694C
  dataRow 0x3900
  dataRow 0x694D
  dataRow 0x3945
  dataRow 0x694F
  dataRow 0x3945
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x323E
  dataRow 0x6900
  dataRow 0x323E
  dataRow 0x6943
  dataRow 0x3700
  dataRow 0x6945
  dataRow 0x3700
  dataRow 0x6948
  dataRow 0x3743
  dataRow 0x6943
  dataRow 0x3743
  dataRow 0x694D
  dataRow 0x3000
  dataRow 0x694C
  dataRow 0x3000
  dataRow 0x694A
  dataRow 0x303C
  dataRow 0x6948
  dataRow 0x303C
  dataRow 0x6943
  dataRow 0x3200
  dataRow 0x6940
  dataRow 0x3200
  dataRow 0x6941
  dataRow 0x3400
  dataRow 0x6943
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3500
  dataRow 0x6900
  dataRow 0x3500
  dataRow 0x6943
  dataRow 0x3541
  dataRow 0x6945
  dataRow 0x3541
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x6900
  dataRow 0x3700
  dataRow 0x6943
  dataRow 0x3743
  dataRow 0x6945
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x6948
  dataRow 0x3400
  dataRow 0x694A
  dataRow 0x3440
  dataRow 0x694C
  dataRow 0x3440
  dataRow 0x6948
  dataRow 0x3900
  dataRow 0x6943
  dataRow 0x3900
  dataRow 0x6945
  dataRow 0x3945
  dataRow 0x6943
  dataRow 0x3945
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x6948
  dataRow 0x323E
  dataRow 0x6947
  dataRow 0x323E
  dataRow 0x6948
  dataRow 0x3700
  dataRow 0x6943
  dataRow 0x3700
  dataRow 0x6945
  dataRow 0x3743
  dataRow 0x6948
  dataRow 0x3743
  dataRow 0x694D
  dataRow 0x3000
  dataRow 0x694C
  dataRow 0x3000
  dataRow 0x694D
  dataRow 0x303C
  dataRow 0x694F
  dataRow 0x303C
  dataRow 0x6948
  dataRow 0x3200
  dataRow 0x6900
  dataRow 0x3200
  dataRow 0x694A
  dataRow 0x3400
  dataRow 0x6900
  dataRow 0x3400
  dataRow 0x0000
  dataRow 0x0000
