{-# LANGUAGE RecursiveDo #-}
module GameState where
import TAsm
import TAsmMonad hiding (label)
import Helpers
import Data.Word
import Constants
import Font
import Sound.Draw
import Sound.Win
import Sound.Intro
import Sound.Lose
import Music.Thunderstruct
import Music.NyanCat
import Music.Sandstorm
import Music.Tron
import Music.Test
import Music.Market
import Music.Gym
import Music.Ganondorf
import Music.Livetonight
import Music.Masterofpuppets

data GameState = GameState {
      players            :: [Player]
    , paused             :: Position
    , gameSpeed          :: Position
    , roundNumber        :: Position
    , numberOfPlayers    :: Position
    , gameOver           :: Position
    , createHoles        :: Position
    , fontBase           :: Position
    , gameSpeedArray     :: Position
    , gameString         :: [Position]
    , gameStringColor    :: [Position]
    , gameMidString      :: [Position]
    , gameMidStringColor :: [Position]
    , songArray          :: [Position]
    , soundEffectArray   :: [Position]
    , soundEffect        :: Position
    , frameBuffer        :: Position
    , song               :: Position
    , songPosition       :: Position
    }

data Player = Player {
      playerX             :: Position
    , playerY             :: Position
    , playerPrevX         :: Position
    , playerPrevY         :: Position
    , playerDir           :: Position
    , playerScore         :: Position
    , playerAlive         :: Position
    , playerColor         :: Position
    , playerSymbol        :: Position
    , originalPlayerState :: ((Word16,Word16), Word16, Word16)
    , playerKeys          :: Keys
    , playerHeadColor     :: Position
    }

data Keys = Keys {
    keyUp    :: Word16
  , keyDown  :: Word16
  , keyLeft  :: Word16
  , keyRight :: Word16
  }

player0Keys = Keys {
   keyUp    = c_keyW
 , keyDown  = c_keyS
 , keyLeft  = c_keyA
 , keyRight = c_keyD
 }

player1Keys = Keys {
   keyUp    = c_keyI
 , keyDown  = c_keyK
 , keyLeft  = c_keyJ
 , keyRight = c_keyL
 }

player2Keys = Keys {
   keyUp    = c_keyArrowUp
 , keyDown  = c_keyArrowDown
 , keyLeft  = c_keyArrowLeft
 , keyRight = c_keyArrowRight
 }

player3Keys = Keys {
   keyUp    = c_keyNumpadUp
 , keyDown  = c_keyNumpadDown
 , keyLeft  = c_keyNumpadLeft
 , keyRight = c_keyNumpadRight
 }


createInitialGameState :: Word16 -> TAsm GameState
createInitialGameState fb = mdo
  jmp exit
  -- Initialize game state
  paused'          <- var 0
  gameSpeed'       <- var 1
  roundNumber'     <- var 0
  numberOfPlayers' <- var 4
  gameOver'        <- var 0xFFFF
  frameBuffer'     <- var fb
  createHoles'     <- var 0
  song'            <- var song1
  soundEffect'     <- var effect0
  chars            <- mapM var (replicate 26 $ c_space + fontBase')
  charColors       <- mapM var (replicate 26 c_textColor)
  midChars         <- mapM var (replicate 26 $ c_space + fontBase')
  midCharColors    <- mapM var (replicate 26 c_textColor)

  gameSpeedArray' <- label
  dataRow 100
  dataRow 75
  dataRow 50
  dataRow 25

  song1 <- label
  tron
  song2 <- label
  nyanCat
  song3 <- label
  sandstorm
  song4 <- label
  thunderstruct
  song5 <- label
  test
  song6 <- label
  market
  song7 <- label
  gym
  song8 <- label
  ganondorf
  song9 <- label
  livetonight
  song10 <- label
  masterofpuppets
  song11 <- label
  dataRow 0
  song12 <- label
  dataRow 0
  dataRow 0

  effect0 <- label
  intro
  effect1 <- label
  draw
  effect2 <- label
  win
  effect3 <- label
  lose


  let songArray' = [song1,song2,song3,song4,song5,song6,song7,song8,song9,song10,
                    song11,song12]
  let soundEffectArray' = [effect0, effect1, effect2, effect3]



  -- Create Players
  p0 <- createPlayer (30,8)    c_dirDown c_p0 c_p0Head c_lmbda   player0Keys
  p1 <- createPlayer (30,116)  c_dirUp   c_p1 c_p1Head c_tilde   player1Keys
  p2 <- createPlayer (129,116) c_dirUp   c_p2 c_p2Head c_pi      player2Keys
  p3 <- createPlayer (129,8)   c_dirDown c_p3 c_p3Head c_smiley  player3Keys

  fontBase' <- label
  font

  exit <- label

  return $ GameState {
                     players            = [p0,p1,p2,p3]
                   , paused             = paused'
                   , gameSpeed          = gameSpeed'
                   , roundNumber        = roundNumber'
                   , numberOfPlayers    = numberOfPlayers'
                   , fontBase           = fontBase'
                   , gameOver           = gameOver'
                   , frameBuffer        = frameBuffer'
                   , createHoles        = createHoles'
                   , gameMidString      = midChars
                   , gameMidStringColor = midCharColors
                   , gameString         = chars
                   , gameStringColor    = charColors
                   , gameSpeedArray     = gameSpeedArray'
                   , songPosition       = song'
                   , song               = song'
                   , songArray          = songArray'
                   , soundEffect        = soundEffect'
                   , soundEffectArray   = soundEffectArray'
                   }


createPlayer :: (Word16, Word16) -- |Initial coordinates
             -> Word16 -- |Initial direction
             -> Word16 -- |Color
             -> Word16 -- |Head Color
             -> Word16 -- |Symbol (like c_lmbda)
             -> Keys   -- |
             -> TAsm Player
createPlayer (x,y) d c hc s k = do
    playerX'        <- var x
    playerY'        <- var y
    playerPrevX'    <- var x
    playerPrevY'    <- var y
    playerDir'      <- var d
    playerScore'    <- var 0
    playerAlive'    <- var 1
    playerColor'    <- var c
    playerHeadColor' <- var hc
    playerSymbol'   <- var s

    return $ Player {
      playerX             = playerX'
    , playerY             = playerY'
    , playerPrevX         = playerPrevX'
    , playerPrevY         = playerPrevY'
    , playerDir           = playerDir'
    , playerScore         = playerScore'
    , playerAlive         = playerAlive'
    , playerColor         = playerColor'
    , playerSymbol        = playerSymbol'
    , originalPlayerState = ((x,y),d,c)
    , playerKeys          = k
    , playerHeadColor     = playerHeadColor'
   }

resetGameState :: GameState -> TAsm ()
resetGameState gs = using [gr0] $ do
  mapM_ resetPlayerState (players gs)


resetPlayerState :: Player -> TAsm ()
resetPlayerState p = using [gr0] $ mdo
  let ((x,y),d,c) = (originalPlayerState p)
  load gr0 x
  store gr0 (playerX p)
  store gr0 (playerPrevX p)

  load gr0 y
  store gr0 (playerY p)
  store gr0 (playerPrevY p)

  load gr0 c
  store gr0 (playerColor p)

  load gr0 d
  store gr0 (playerDir p)

  load gr0 0xFFFF
  store gr0 (playerAlive p)
