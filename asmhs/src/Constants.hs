module Constants where
import Data.Word

c_logoBG = c_boardBgColor
c_logoFG = c_textColor

c_speedBarRow   = 119 :: Word16
c_speedBarCol   = 73  :: Word16

c_speedBarColor   = c_p1           :: Word16
c_speedBarBgColor = c_boardBgColor :: Word16
c_boardBgColor    = 0x4848         :: Word16 -- 01 001 000
c_borderColor     = 0x9898         :: Word16 -- 10 011 000
c_deadHeadColor   = 0xF8F8         :: Word16

c_p0 = 0x9090 :: Word16
c_p1 = 0x2626 :: Word16
c_p2 = 0x7272 :: Word16
c_p3 = 0x4E4E :: Word16

c_p0Head = 0xD2D2 :: Word16
c_p1Head = 0x1414 :: Word16
c_p2Head = 0x7070 :: Word16
c_p3Head = 0x0F0F :: Word16

c_textColor = 0xFCFC :: Word16 -- 11 111 100

c_white  = 0xFFFF :: Word16
c_red    = 0x0707 :: Word16
c_brown  = 0x0D0D :: Word16
c_orange = 0x2F2F :: Word16
c_yellow = 0x3F3F :: Word16
c_green  = 0x3838 :: Word16
c_cyan   = 0xF8F8 :: Word16
c_blue   = 0xC0C0 :: Word16
c_purple = 0xC7C7 :: Word16
c_pink   = 0xEFEF :: Word16

c_keyArrowUp    = 0x4075 :: Word16
c_keyArrowDown  = 0x4072 :: Word16
c_keyArrowLeft  = 0x406B :: Word16
c_keyArrowRight = 0x4074 :: Word16

c_keyW  = 0x001D :: Word16
c_keyS  = 0x001B :: Word16
c_keyA  = 0x001C :: Word16
c_keyD  = 0x0023 :: Word16

c_keyI  = 0x0043 :: Word16
c_keyJ  = 0x003B :: Word16
c_keyK  = 0x0042 :: Word16
c_keyL  = 0x004B :: Word16

c_keyNumpadUp    = 0x0075 :: Word16
c_keyNumpadDown  = 0x0073 :: Word16
c_keyNumpadLeft  = 0x006B :: Word16
c_keyNumpadRight = 0x0074 :: Word16

c_keyPageUp     =   0x407D  :: Word16
c_keyPageDown   =   0x407A  :: Word16


c_keyPlus  = 0x0079 :: Word16
c_keyMinus = 0x007B :: Word16
c_keySpace = 0x0029 :: Word16
c_keyEsc   = 0x0076 :: Word16

c_dirUp    = 0x0001 :: Word16
c_dirDown  = 0x0010 :: Word16
c_dirLeft  = 0x0100 :: Word16
c_dirRight = 0x1000 :: Word16

c_tumblr   = 0x5151 :: Word16
c_youtube  = 0x0e0e :: Word16
c_facebook = 0x9191 :: Word16
c_twitter  = 0xe8e8 :: Word16

c_keyF1  = 0x0005  :: Word16
c_keyF2  = 0x0006  :: Word16
c_keyF3  = 0x0004  :: Word16
c_keyF4  = 0x000C  :: Word16
c_keyF5  = 0x0003  :: Word16
c_keyF6  = 0x000B  :: Word16
c_keyF7  = 0x0083  :: Word16
c_keyF8  = 0x000A  :: Word16
c_keyF9  = 0x0001  :: Word16
c_keyF10 = 0x0009  :: Word16
c_keyF11 = 0x0078  :: Word16
c_keyF12 = 0x0007  :: Word16



tone_off = 0   :: Word16
tone_c0  = 1   :: Word16
tone_ci0 = 2   :: Word16
tone_d0  = 3   :: Word16
tone_di0 = 4   :: Word16
tone_e0  = 5   :: Word16
tone_f0  = 6   :: Word16
tone_fi0 = 7   :: Word16
tone_g0  = 8   :: Word16
tone_gi0 = 9   :: Word16
tone_a0  = 10  :: Word16
tone_ai0 = 11  :: Word16
tone_b0  = 12  :: Word16
tone_c1  = 13  :: Word16
tone_ci1 = 14  :: Word16
tone_d1  = 15  :: Word16
tone_di1 = 16  :: Word16
tone_e1  = 17  :: Word16
tone_f1  = 18  :: Word16
tone_fi1 = 19  :: Word16
tone_g1  = 20  :: Word16
tone_gi1 = 21  :: Word16
tone_a1  = 22  :: Word16
tone_ai1 = 23  :: Word16
tone_b1  = 24  :: Word16
tone_c2  = 25  :: Word16
tone_ci2 = 26  :: Word16
tone_d2  = 27  :: Word16
tone_di2 = 28  :: Word16
tone_e2  = 29  :: Word16
tone_f2  = 30  :: Word16
tone_fi2 = 31  :: Word16
tone_g2  = 32  :: Word16
tone_gi2 = 33  :: Word16
tone_a2  = 34  :: Word16
tone_ai2 = 35  :: Word16
tone_b2  = 36  :: Word16
tone_c3  = 37  :: Word16
tone_ci3 = 38  :: Word16
tone_d3  = 39  :: Word16
tone_di3 = 40  :: Word16
tone_e3  = 41  :: Word16
tone_f3  = 42  :: Word16
tone_fi3 = 43  :: Word16
tone_g3  = 44  :: Word16
tone_gi3 = 45  :: Word16
tone_a3  = 46  :: Word16
tone_ai3 = 47  :: Word16
tone_b3  = 48  :: Word16
tone_c4  = 49  :: Word16
tone_ci4 = 50  :: Word16
tone_d4  = 51  :: Word16
tone_di4 = 52  :: Word16
tone_e4  = 53  :: Word16
tone_f4  = 54  :: Word16
tone_fi4 = 55  :: Word16
tone_g4  = 56  :: Word16
tone_gi4 = 57  :: Word16
tone_a4  = 58  :: Word16
tone_ai4 = 59  :: Word16
tone_b4  = 60  :: Word16
tone_c5  = 61  :: Word16
tone_ci5 = 62  :: Word16
tone_d5  = 63  :: Word16
tone_di5 = 64  :: Word16
tone_e5  = 65  :: Word16
tone_f5  = 66  :: Word16
tone_fi5 = 67  :: Word16
tone_g5  = 68  :: Word16
tone_gi5 = 69  :: Word16
tone_a5  = 70  :: Word16
tone_ai5 = 71  :: Word16
tone_b5  = 72  :: Word16
tone_c6  = 73  :: Word16
tone_ci6 = 74  :: Word16
tone_d6  = 75  :: Word16
tone_di6 = 76  :: Word16
tone_e6  = 77  :: Word16
tone_f6  = 78  :: Word16
tone_fi6 = 79  :: Word16
tone_g6  = 80  :: Word16
tone_gi6 = 81  :: Word16
tone_a6  = 82  :: Word16
tone_ai6 = 83  :: Word16
tone_b6  = 84  :: Word16
tone_c7  = 85  :: Word16
tone_ci7 = 86  :: Word16
tone_d7  = 87  :: Word16
tone_di7 = 88  :: Word16
tone_e7  = 89  :: Word16
tone_f7  = 90  :: Word16
tone_fi7 = 91  :: Word16
tone_g7  = 92  :: Word16
tone_gi7 = 93  :: Word16
tone_a7  = 94  :: Word16
tone_ai7 = 95  :: Word16
tone_b7  = 96  :: Word16
tone_c8  = 97  :: Word16
tone_ci8 = 98  :: Word16
tone_d8  = 99  :: Word16
tone_di8 = 100 :: Word16
tone_e8  = 101 :: Word16
tone_f8  = 102 :: Word16
tone_fi8 = 103 :: Word16
tone_g8  = 104 :: Word16
tone_gi8 = 105 :: Word16
tone_a8  = 106 :: Word16
tone_ai8 = 107 :: Word16
tone_b8  = 108 :: Word16
