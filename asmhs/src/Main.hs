import TAsmMonad
import TAsm
import AssemblyMain

main = putStr $ unlines $ map showRowBinary $ assemble $ asmMain
