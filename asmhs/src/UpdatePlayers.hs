{-# LANGUAGE RecursiveDo #-} -- |Main assembly module
module UpdatePlayers where
import TAsm
import TAsmMonad hiding (label)
import Control.Monad
import Constants
import Helpers
import GameState

-- |Using gr15 to count number of alive players
updatePlayer :: Player -> GameState -> TAsm()
updatePlayer player state = using rs $ mdo

  --Check if the player is alive
  load reg1 (playerAlive player) <| indirect
  or' reg1 reg1
  jz exit

  --Count number of alive players
  load reg1 1
  add gr15 reg1
  load gr14 (playerScore player)
  load gr13 (playerSymbol player) <| indirect


  --Check if we should create a hole
  load reg1 (createHoles state) <| indirect
  or' reg1 reg1
  jz skip

  --Create hole
  load dir (frameBuffer state) <| indirect
  load reg1 (playerPrevX player) <| indirect
  load row (playerPrevY player) <| indirect
  load color c_boardBgColor
  drawPixel reg1 row color dir

  skip <- label

  -- Update Prev Position
  load reg1 (playerY player) <| indirect
  store reg1 (playerPrevY player)
  load reg1 (playerX player) <| indirect
  store reg1 (playerPrevX player)

  ------------------------
  -- Compare Directions --
  ------------------------
  load dir (playerDir player) <| indirect

  load reg1 c_dirUp
  jeq dir reg1 up
  load reg1 c_dirDown
  jeq dir reg1 down
  load reg1 c_dirLeft
  jeq dir reg1 left
  load reg1 c_dirRight
  jeq dir reg1 right
  jmp exit

  ---------------------
  -- Update Position --
  ---------------------
  up <- label
  load reg1 (playerY player) <| indirect
  dec reg1
  store reg1 (playerY player)
  jmp validMove

  down <- label
  load reg1 (playerY player) <| indirect
  inc reg1
  store reg1 (playerY player)
  jmp validMove

  left <- label
  load reg1 (playerX player) <| indirect
  dec reg1
  store reg1 (playerX player)
  jmp validMove

  right <- label
  load reg1 (playerX player) <| indirect
  inc reg1
  store reg1 (playerX player)
  jmp validMove

  -------------------------
  -- Check if valid move --
  -------------------------
  validMove <- label
  --Calculate memory address
  load reg1(playerX player) <| indirect
  load row (playerY player) <| indirect
  load color (frameBuffer state) <| indirect
  load dir 160
  mulu row dir
  add row reg1
  add row color
  load' color row

  --Odd or even address
  load dir 1
  and' dir row
  jz even

 --Odd
  load dir 0x00FF
  and' color dir
  load reg1 c_boardBgColor
  and' reg1 dir
  jeq color reg1 draw
  jmp kill

  --Even
  even <- label
  load dir 0xFF00
  and' color dir
  load reg1 c_boardBgColor
  and' reg1 dir
  jeq color reg1 draw
  jmp kill

  -----------------
  -- Kill player --
  -----------------
  kill <- label
  load reg1 0x0000
  store reg1 (playerAlive player)
  --Play sound
  load reg1 (soundEffectArray state !! 3)
  store reg1 (soundEffect state)
  load reg1 2
  glob timer3 reg1


  --Draw collision
  load dir (frameBuffer state) <| indirect
  load reg1 (playerPrevX player) <| indirect
  load row (playerPrevY player) <| indirect
  load color c_deadHeadColor
  drawPixel reg1 row color dir
  jmp exit


  ----------
  -- Draw --
  ----------
  draw <- label
  load dir (frameBuffer state) <| indirect
  --Draw head
  load reg1 (playerX player) <| indirect
  load row (playerY player) <| indirect
  load color (playerHeadColor player) <| indirect
  drawPixel reg1 row color dir
  --Draw tail
  load reg1 (playerPrevX player) <| indirect
  load row (playerPrevY player) <| indirect
  load color (playerColor player) <| indirect
  drawPixel reg1 row color dir

  exit <- label

  return()
  where rs@[dir, reg1, row, color] = take 4 $ getFree[]
