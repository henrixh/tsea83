{-# LANGUAGE RecursiveDo #-} -- |Main assembly module
module Interupts where
import TAsm
import TAsmMonad hiding (label)
import Control.Monad
import Constants
import Board
import Ps2
import Helpers
import Font
import FontTools
import GameState
import UpdatePlayers

setupInterupts :: GameState
           -> TAsm ()
setupInterupts state = using rs $ mdo


  --Game loop timer
  load reg1 (gameSpeed state) <| indirect
  load reg2 (gameSpeedArray state)
  add reg2 reg1
  add reg2 reg1
  load' reg1 reg2
  glob timer1 reg1

  --Music timer
  load reg1 0x0069
  glob timer2 reg1

  --Set interupt vector
  load reg1 interuptLoc
  glob interuptVector reg1

  --Pause game
  load reg1 0xFFFF
  store reg1 (paused state)

  --Enable interupts
  glob interuptEnable reg1

  jmp exit

  -----------------------
  -- Interupt Routines --
  -----------------------
  ps2Intr <- label
  ps2InteruptRoutine state
  rti
  timer1Intr <- label
  timer1InteruptRoutine state
  rti
  timer2Intr <- label
  timer2InteruptRoutine state
  rti
  timer3Intr <- label
  timer3InteruptRoutine state
  rti
  timer4Intr <- label
  timer4InteruptRoutine
  rti

  ---------------------
  -- Interupt Vector --
  ---------------------
  interuptLoc <- label
  dataRow ps2Intr
  dataRow timer1Intr
  dataRow timer2Intr
  dataRow timer3Intr
  dataRow timer4Intr

  exit <- label

  return ()
  where rs@[reg1, reg2] = take 2 $ getFree []


----------------
-- Game timer --
----------------
timer1InteruptRoutine :: GameState
                      -> TAsm()
timer1InteruptRoutine state = using rs $ mdo
  jmp begin

  --Variable to count turns
  counter <- label
  dataRow 0

  begin <- label

  --Push registers
  push gr14
  push gr13

  --Exit if game is paused
  load reg1 (paused state) <| indirect
  or' reg1 reg1
  jnz exit

  --------------------------
  -- Handle hole creation --
  --------------------------
  load reg1 16 -- How often we want holes
  load reg2 counter <| indirect
  inc reg2
  store reg2 counter
  jeq reg2 reg1 addHoles
  jmp updatePlayers

  addHoles <- label
  -- Set create holes variable
  load reg1 0
  store reg1 counter
  load reg1 0xFFFF
  store reg1 (createHoles state)


  --------------------
  -- Update players --
  --------------------
  updatePlayers <- label
  load gr14 0x0000
  load gr15 0x0000
  mapM_ (\x -> updatePlayer x state) (players state)

  --Reset create holes flag
  load reg1 0
  store reg1 (createHoles state)

  --Check victory
  lsr gr15 1
  or' gr15 gr15
  jnz exit

  --------------
  -- GameOver --
  --------------

  -- Set paused and game over variables
  load reg1 0xFFFF
  store reg1 (paused state)
  store reg1 (gameOver state)

  -- Update round number
  load reg1 (roundNumber state) <| indirect
  inc reg1
  store reg1 (roundNumber state)

  --Update score
  or' gr14 gr14
  jz draw
  load' reg1 gr14
  inc reg1
  store' gr14 reg1

  --Player wins
  --Play sound
  load reg1 ((soundEffectArray state) !! 2)
  store reg1 (soundEffect state)
  load reg1 2
  glob timer3 reg1
  --Draw text
  load reg1 fb
  add gr13 reg1
  load' reg1 gr13
  store gr13 ((gameMidString state) !! 9)
  load reg1 (c_space + fb)
  store reg1 ((gameMidString state) !! 10)
  load reg1 (c_W + fb)
  store reg1 ((gameMidString state) !! 11)
  load reg1 (c_I + fb)
  store reg1 ((gameMidString state) !! 12)
  load reg1 (c_N + fb)
  store reg1 ((gameMidString state) !! 13)
  load reg1 (c_S + fb)
  store reg1 ((gameMidString state) !! 14)
  load reg1 (c_exclamation + fb)
  store reg1 ((gameMidString state) !! 15)
  printfMid state
  jmp exit

  --Draw
  draw <- label
  --Play sound effect
  load reg1 ((soundEffectArray state) !! 1)
  store reg1 (soundEffect state)
  load reg1 2
  glob timer3 reg1
  --Draw text
  load reg1 (c_D + fb)
  store reg1 ((gameMidString state) !! 11)
  load reg1 (c_R + fb)
  store reg1 ((gameMidString state) !! 12)
  load reg1 (c_A + fb)
  store reg1 ((gameMidString state) !! 13)
  load reg1 (c_W + fb)
  store reg1 ((gameMidString state) !! 14)
  load reg1 (c_exclamation + fb)
  store reg1 ((gameMidString state) !! 15)
  printfMid state
  jmp exit


  --Exit
  exit <- label
  pop gr13
  pop gr14

  return ()
  where rs@[reg1, reg2] = take 2 $ getFree []
        fb = fontBase state


----------------
-- Play music --
----------------
timer2InteruptRoutine :: GameState -> TAsm()
timer2InteruptRoutine state = using rs $ mdo

  -- Load note, increase pos
  load pos (songPosition state) <| indirect
  load temp (song state) <| indirect

  -- Handle pulse widths
  load' note pos
  lsr note 8
  load temp 0x00FF

  sub temp note
  jnz pwDone

  load' note pos
  inc pos
  inc pos
  store note pulseWidthData
  jmp pwDone

  pulseWidthData <- var 0
  pwDone <- label

  -- Load pulse width
  load pw pulseWidthData <| indirect
  lsl pw 8

  -- Load note
  load' note pos

  -- Play sound 1
  load temp 0x00FF
  and' note temp
  or' note pw -- Pulse width
  glob sound1 note

  lsr pw 2 -- Update pulse width
  load temp 0xFF00
  and' pw temp


  -- Set timer
  load' note pos
  lsr note 8
  glob timer2 note

  --Load next notes
  inc pos
  inc pos

  -- Play sound 2
  load' note pos
  load temp 0x00FF
  and' note temp
  glob sound2 note

  lsr pw 2 -- Update pulse width
  load temp 0xFF00
  and' pw temp

  -- Play sound 3
  load' note pos
  lsr note 8
  or' note pw
  glob sound3 note


  --Time to check for loops
  inc pos
  inc pos
  load' note pos
  or' note note
  jnz exit
  load pos (song state) <| indirect

  exit <- label
  --Store position
  store pos (songPosition state)

  return ()
  where rs@[pos, note, temp, pw] = take 4 $ getFree []

-----------------------
-- Play sound effect --
-----------------------
timer3InteruptRoutine :: GameState -> TAsm()
timer3InteruptRoutine state = using rs $ mdo

  load pos (soundEffect state) <| indirect
  load' note pos
  glob sevenSeg note
  load mask 0x00FF
  and' note mask
  glob sound4 note

  load' note pos
  lsr note 8
  glob timer3 note

  inc pos
  inc pos
  store pos (soundEffect state)

  return ()
  where rs@[pos, note, mask] = take 3 $ getFree []

timer4InteruptRoutine :: TAsm()
timer4InteruptRoutine = mdo
  return ()
