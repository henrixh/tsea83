{-# LANGUAGE RecursiveDo #-}
-- |Main assembly module
module AssemblyMain (asmMain) where
import TAsm
import TAsmMonad hiding (label)
import Control.Monad
import Constants
import Board
import Helpers
import Interupts
import GameState
import DrawLogo

-- |Main entry point for the assembly program
asmMain :: TAsm ()
asmMain = mdo
  -- Set up the stack pointer
  load gr0 (0xFFFE)
  ssp gr0

  -- Generate the game state
  state <- createInitialGameState framebuffer

  -- Set up framebuffer
  load gr0 framebuffer
  glob gpuBase gr0

  drawLogo state

  -- Initialize the interupt routines
  setupInterupts state

  -- Unpause
  load gr0 0x0000
 -- store gr0 (paused state)

  -- Kill program flow, LONG LIVE THE INTERUPTS!
  loop <- label
  jmp loop

  -- Put the framebuffer after all other code
  framebuffer <- label

  return ()
