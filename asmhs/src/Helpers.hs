{-# LANGUAGE RecursiveDo #-}
module Helpers where

import TAsm
import qualified Data.List as L

import TAsmMonad hiding (label)
import Data.Word
import Font

-- |Run action with given registers on stack
using :: [Reg] -> TAsm () -> TAsm ()
using rs mf = do
  mapM_ push rs
  mf
  mapM_ pop (reverse rs)


-- |Get a list registers not in the argument list
getFree :: [Reg] -> [Reg]
getFree xs = L.deleteFirstsBy (==) [gr0
          ,gr1
          ,gr2
          ,gr3
          ,gr4
          ,gr5
          ,gr6
          ,gr7
          ,gr8
          ,gr9
          ,gr10
          ,gr11
          ,gr12
          ,gr13
          ,gr14
          ,gr15
          ] xs

-- |Load the first argument with the data on the adress the second argument
-- contains.
load' :: Reg -> Reg -> TAsm ()
load' dest srcAdr = do
  l <- label
  store srcAdr (l + 6)
  load dest 0 <| indirect

--  |Store the data in the second argument to the adress in the first argument
store' :: Reg -> Reg -> TAsm ()
store' destAdr d = do
  l <- label
  store destAdr (l + 6)
  store d 0

-- | Draw a single pixel, preserving its neighbour
drawPixel :: Reg -- | Column
          -> Reg -- | Row
          -> Reg -- | Color
          -> Reg -- | gpu base
          -> TAsm()
drawPixel column rw color gb = using (rs ++ [column, color]) $ mdo
  -- Calculate effective adress
  -- adr := rw * 160 + column + gb
  load creg 160
  cp adr rw
  mulu adr creg
  add adr column
  add adr gb

  -- Get the previous color in a register
  load' prevcol adr

  -- Determine odd/evenness
  load creg 1
  and' column creg
  jz even

  -- Odd
  load creg 0x00FF
  and' color creg

  load creg 0xFF00
  and' prevcol creg

  or' color prevcol

  jmp exit

  -- Even
  even <- label

  load creg 0xFF00
  and' color creg

  load creg 0x00FF
  and' prevcol creg

  or' color prevcol

  -- Done. GTFO
  exit <- label
  store' adr color

  where rs@(creg:adr:prevcol:[]) = take 3 $ getFree [column, rw, color, (gb)]

incColor :: Reg -> TAsm ()
incColor r = using [one] $ do
  load one 0x0101
  add r one
    where one = head $ getFree [r]

incAdr :: Reg -> TAsm ()
incAdr r = using [one] $ do
  load one 1
  add r one
    where one = head $ getFree [r]

inc :: Reg -> TAsm ()
inc r = using [one] $ do
  load one 1
  add r one
    where one = head $ getFree [r]

dec :: Reg -> TAsm ()
dec r = using [one] $ do
  load one 1
  sub r one
    where one = head $ getFree [r]

asSubrutine :: TAsm () -> TAsm Position
asSubrutine f = mdo
  jmp ret
  p <- label
  f
  rts
  ret <- label
  return p

-- | Draw a single character, using drawPixel
drawChar :: Reg -- | Column
         -> Reg -- | Row
         -> Reg -- | Pointer to Char
         -> Reg -- | Color
         -> Reg -- | gpu base
         -> TAsm()
drawChar column rw char color gb = using ([column, color, ffff, cjmp]) $
  mdo
    load ffff 0xFFFF

    begin <- label
    load' cjmp char
    jeq ffff cjmp exit
    add column cjmp
    drawPixel column rw color gb
    inc char
    inc char
    jmp begin
    
    exit <- label
    nop
    where ffff = head ( getFree [column, rw, char, color, gb])
          cjmp = head ( getFree [column, rw, char, color, gb, ffff])

var :: Word16 -> TAsm Position
var d = do
  p <- label
  dataRow d
  return p
-- | Draw a pair of pixels
drawPixelPair :: Reg -- | Column
              -> Reg -- | Row
              -> Reg -- | Color
              -> Reg -- | gpu base
              -> TAsm()
drawPixelPair column rw color gb = using (rs ++ [column, color]) $ mdo
  -- Calculate effective adress
  -- adr := rw * 160 + column + gb
  load creg 160
  cp adr rw
  mulu adr creg
  add adr column
  add adr gb

  store' adr color

  where rs@(creg:adr:prevcol:[]) = take 3 $ getFree [column, rw, color, (gb)]

