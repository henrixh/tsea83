{-# LANGUAGE RecursiveDo #-}
-- |Helper functions for board drawing
module Board where

import TAsm
import TAsmMonad hiding (label)
import Data.Word
import GameState
import Font
import FontTools
import Constants

import Control.Monad
import Helpers

setupBoard :: GameState -- |GameState
           -> TAsm ()
setupBoard state = using rs $ mdo
  load gb (frameBuffer state) <| indirect
  load textBG c_borderColor
  load boardBG c_boardBgColor
  drawBoard textBG boardBG gb state
  drawSpeedBar state

   where rs@[textBG, boardBG, gb] = take 3 $ getFree []


-- |Draw the board from a GameState
drawBoard :: Reg -- |Text background color
          -> Reg -- |Board BG color
          -> Reg -- |GPUBase
          -> GameState
          -> TAsm ()
drawBoard textBG boardBG gb gstate = using (rs ++ [textBG,boardBG,gb]) $
  mdo
    drawBlob 0 6 textBG gb
    drawBlob 6 118 boardBG gb
    drawDoubleBorder 6 gb textBG
    updateString gstate
    printf gstate

     where rs@[tmp] = take 1 $ getFree [textBG,boardBG, gb]

updateString :: GameState -> TAsm ()
updateString state = using rs $ mdo
  let numberLookup = 0
  -- Player Zero
  updatePlayerString
    ((players state) !! 0)
    ((gameString state) !! 0)
    ((gameStringColor state) !! 0)
    (fontBase state)

  -- Player One
  updatePlayerString
    ((players state) !! 1)
    ((gameString state) !! 4)
    ((gameStringColor state) !! 4)
    (fontBase state)

  -- Player Two
  updatePlayerString
    ((players state) !! 2)
    ((gameString state) !! 19)
    ((gameStringColor state) !! 19)
    (fontBase state)

  -- Player Three
  updatePlayerString
    ((players state) !! 3)
    ((gameString state) !! 23)
    ((gameStringColor state) !! 23)
    (fontBase state)

  -- Put ROUND
  load symbol (c_R + fb)
  store symbol ((gameString state) !! 9)
  load symbol (c_O + fb)
  store symbol ((gameString state) !! 10)
  load symbol (c_U + fb)
  store symbol ((gameString state) !! 11)
  load symbol (c_N + fb)
  store symbol ((gameString state) !! 12)
  load symbol (c_D + fb)
  store symbol ((gameString state) !! 13)
  load symbol (c_colon + fb)
  store symbol ((gameString state) !! 14)

  -- Put numbers
  let number = symbol
  pn <- asSubrutine $ putNumber (fontBase state) strp number

  -- Put round number
  load strp ((gameString state) !! 15)
  load number (roundNumber state) <| indirect
  jsr pn

  -- Put the players
  -- P0
  load strp ((gameString state) !! 1)
  load number (playerScore ((players state) !! 0)) <| indirect
  jsr pn

  -- P1
  load strp ((gameString state) !! 5)
  load number (playerScore ((players state) !! 1)) <| indirect
  jsr pn

  -- P2
  load strp ((gameString state) !! 20)
  load number (playerScore ((players state) !! 2)) <| indirect
  jsr pn

  -- P4
  load strp ((gameString state) !! 24)
  load number (playerScore ((players state) !! 3)) <| indirect
  jsr pn


  where rs@[symbol,strp] = take 2 $ getFree []
        fb = fontBase state

putNumber :: Position -- | FontBase
             -> Reg -- | Pointer to string position
             -> Reg -- | Number to put
             -> TAsm ()
putNumber fb strp n = using ([n] ++ rs) $ mdo
  -- Create lookup Table
  let ns = [c_0, c_1, c_2, c_3, c_4, c_5, c_6, c_7, c_8, c_9, c_A, c_B, c_C, c_D, c_E, c_F]
  numberLookup <- label
  mapM_ dataRow ns

  load fbreg fb
  -- Remove upper byte
  load tmp 0x00FF
  and' n tmp

  -- Store away a copy.
  cp tmp n

  ----------------
  -- Print msbs --
  ----------------

  -- Get the correct bits
  lsr n 4
  -- Resolve lookup
  load charp numberLookup
  add charp n
  add charp n

  -- Load with actual data
  load' charp charp
  add charp fbreg

  -- Store the number
  store' strp charp

  ----------------
  -- Print lsbs --
  ----------------
  -- Get the correct bits
  cp n tmp
  load tmp 0x000F
  and' n tmp

  -- Resolve lookup
  load charp numberLookup
  add charp n
  add charp n

  -- Load with actual data
  load' charp charp
  add charp fbreg

  -- Store the number
  load tmp 2
  add strp tmp
  store' strp charp


  where rs@[charp, fbreg, tmp, tmp2] = take 4 $ getFree [strp,n]


updatePlayerString ::
                   Player -- | Player to print
                   -> Position -- | String position
                   -> Position -- | Color position
                   -> Position -- | FontBase
                   -> TAsm ()
updatePlayerString p strpos colorpos fb = using rs $ mdo
  -- Put player symbol
  load symbol (playerSymbol p) <| indirect
  load fbase fb
  add symbol fbase
  store symbol strpos

  load symbol (playerColor p) <| indirect
  store symbol colorpos

    ---- Put space
  load symbol (c_lmbda + fb)
  store symbol (strpos + 2)

  where rs@[symbol, fbase] = take 2 $ getFree []

drawBlob :: Word16 -- |First row
         -> Word16 -- |Last row
         -> Reg -- |Color
         -> Reg -- |GPUBase
         -> TAsm ()
drawBlob first last color gb = using rs $
  mdo
    load ix $ 160 * first
    load goal $ 160 * last

    add ix gb
    add goal gb

    loop <- label
    store' ix color
    incAdr ix

    jeq ix goal exit
    jmp loop

    exit <- label
    return ()

   where rs@[ix, goal] = take 2 $ getFree [color, gb]


-- |Draw a border with a number of reserved pixels on the top
drawBorder :: Word16 -- |Reserved pixels
           -> Reg -- |GPUBase
           -> Reg -- |Register containing desired color
           -> TAsm ()
drawBorder reserved pos color = using (rs ++ [pos,color]) $ mdo
  jmp begin

  -- Setup dPixel subroutine
  dp <- asSubrutine (drawPixel column rw color pos)
  let dPixel = jsr dp

  begin <- label

  -- Draw upper line
  load hr 160
  load rw reserved
  load column 0

  loopUpper <- label
  jeq hr column doneUpper
  dPixel
  inc column
  jmp loopUpper
  doneUpper <- label

  load rw reserved
  load column 159

  -- Draw right border
  load column 159
  load rw reserved

  loopRight <- label
  jeq hr rw doneRight
  dPixel
  inc rw
  jmp loopRight
  doneRight <- label

  -- Draw lower line
  load hr 160
  load rw 119
  load column 0

  loopLower <- label
  jeq hr column doneLower
  dPixel
  inc column
  jmp loopLower
  doneLower <- label

  -- Draw left border
  load column 0
  load rw reserved
  load hr 120

  loopLeft <- label
  jeq hr rw doneLeft
  dPixel
  inc rw
  jmp loopLeft
  doneLeft <- label

  return ()
    where rs@[column, rw, hr] = take 3 $ getFree [pos, color]

-- |Draw a border with a number of reserved pixels on the top
drawDoubleBorder :: Word16 -- |Reserved pixels
           -> Reg -- |GPUBase
           -> Reg -- |Register containing desired color
           -> TAsm ()
drawDoubleBorder reserved pos color = using (rs ++ [pos,color]) $ mdo
  jmp begin

  -- Setup dPixel subroutine
  dp <- asSubrutine (drawPixelPair column rw color pos)
  let dPixel = jsr dp

  begin <- label

  -- Draw upper line
  load hr 160
  load rw reserved
  load column 0

  loopUpper <- label
  jeq hr column doneUpper
  dPixel
  inc column
  inc column
  jmp loopUpper
  doneUpper <- label

  inc rw

  loopUpper2 <- label
  jeq hr column doneUpper2
  dPixel
  inc column
  inc column
  jmp loopUpper2
  doneUpper2 <- label

  load rw reserved
  load column 159

  -- Draw right border
  load column 159
  load hr 119
  load rw reserved

  loopRight <- label
  jeq hr rw doneRight
  dPixel
  inc rw
  jmp loopRight
  doneRight <- label

  -- Draw lower line
  load hr 160
  load rw 119
  load column 0

  loopLower <- label
  jeq hr column doneLower
  dPixel
  inc column
  jmp loopLower
  doneLower <- label

  dec rw
  load column 0

  loopLower2 <- label
  jeq hr column doneLower2
  dPixel
  inc column
  jmp loopLower2
  doneLower2 <- label


  -- Draw left border
  load column 0
  load rw reserved
  load hr 120

  loopLeft <- label
  jeq hr rw doneLeft
  dPixel
  inc rw
  jmp loopLeft
  doneLeft <- label

  return ()
    where rs@[column, rw, hr] = take 3 $ getFree [pos, color]

drawSpeedBar :: GameState -> TAsm()
drawSpeedBar state = using rs $ mdo
  load counter 0x0000
  load speed (gameSpeed state) <| indirect
  inc speed
  lsl speed 2
  load gb (frameBuffer state) <| indirect
  load row c_speedBarRow
  load col c_speedBarCol

  --Draw bar
  loop <- label
  jeq speed counter loop2
  load speed c_speedBarColor
  drawPixel col row speed gb
  load speed (gameSpeed state) <| indirect
  inc speed
  lsl speed 2
  inc col
  inc counter
  jmp loop

  --Draw background
  loop2 <- label
  load speed 0x0010
  jeq counter speed exit
  load speed c_speedBarBgColor
  drawPixel col row speed gb
  inc counter
  inc col
  jmp loop2


  exit <- label
  return ()
  where rs@[speed, row, col, gb, counter] = take 5 $ getFree []


resetGame :: GameState -> TAsm()
resetGame state = using rs $ mdo
  --Set Gameover
  load reg1 0xFFFF
  store reg1 (gameOver state)
  --Reset game speed
  load reg1 1
  store reg1 (gameSpeed state)

  --Reset music
  load reg1 (songArray state !! 0)
  store reg1 (song state)
  store reg1 (songPosition state)

  load reg1 0
  --Disable interrupts and reset scores
  glob interuptEnable reg1
  store reg1 (roundNumber state)
  mapM_(resetPlayerScore)(players state)
  --Add push 0x0000 to the stack
  push reg1
  push reg1
  push reg1
  --Try to return from interrupt which will set PC to 0
  rti
  where rs@[reg1] = take 1 $ getFree []

resetPlayerScore :: Player -> TAsm()
resetPlayerScore p = using rs $ mdo
  load reg1 0
  store reg1 (playerScore p)
  where rs@[reg1] = take 1 $ getFree []
