{-# LANGUAGE RecursiveDo #-}
-- |Main draw module
module Sound.Draw where
import TAsm

-- |Draw data
draw :: TAsm ()
draw = do
  dataRow 0x7C55
  dataRow 0x7C00
  dataRow 0x7C52
  dataRow 0x7C4E
  dataRow 0x7C00
  dataRow 0x7C54
  dataRow 0x7C50
  dataRow 0x7C00
  dataRow 0x7C4B
  dataRow 0x7C4E
  dataRow 0x7C4D
  dataRow 0x7C4B
  dataRow 0x7C49
  dataRow 0x7C00
  dataRow 0x7C00
  dataRow 0x7C00
  dataRow 0x7C00
  dataRow 0x3E52
  dataRow 0x3E54
  dataRow 0x7C55
  dataRow 0x0000
