{-# LANGUAGE RecursiveDo #-}
-- |Main lose module
module Sound.Lose where
import TAsm

-- |Lose data
lose :: TAsm ()
lose = do
  dataRow 0x7D39
  dataRow 0x7D38
  dataRow 0x7D37
  dataRow 0x0000
