{-# LANGUAGE RecursiveDo #-}
-- |Main intro module
module Sound.Intro where
import TAsm

-- |Intro data
intro :: TAsm ()
intro = do
  dataRow 0xFA40
  dataRow 0x7D40
  dataRow 0xFA43
  dataRow 0x7D43
  dataRow 0x7D00
  dataRow 0x7D4C
  dataRow 0x7D47
  dataRow 0x7D43
  dataRow 0x7D3E
  dataRow 0x7D39
  dataRow 0x7D34
  dataRow 0x7D40
  dataRow 0x7D43
  dataRow 0x7D47
  dataRow 0xFA40
  dataRow 0xFA40
  dataRow 0x0000
