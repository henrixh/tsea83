{-# LANGUAGE RecursiveDo #-}
-- |Main win module
module Sound.Win where
import TAsm

-- |Win data
win :: TAsm ()
win = do
  dataRow 0x3E49
  dataRow 0x3E4B
  dataRow 0x3E4D
  dataRow 0x3E4E
  dataRow 0x3E50
  dataRow 0x3E52
  dataRow 0x3E54
  dataRow 0x7C55
  dataRow 0xBA00
  dataRow 0x7C54
  dataRow 0xBA00
  dataRow 0xF855
  dataRow 0xF855
  dataRow 0xF800
  dataRow 0xBA00
  dataRow 0x0000
