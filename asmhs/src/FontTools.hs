{-# LANGUAGE RecursiveDo #-}
module FontTools where
import Font
import GameState
import Data.Word
import TAsmMonad hiding (label)
import TAsm
import Control.Monad
import Helpers

putString :: Reg -- |GPUBase
          -> Reg -- |Row on which to draw
          -> Position -- |FontBase
          -> [Word16] -- |Chars to draw
          -> [Word16] -- |Desired colors (one per char)
          -> TAsm ()
putString gb rw fb chars colors = using (rs ++ [gb,rw]) $ mdo
    mapM_ putchar $ zip (zip (map (+ fb) chars) colors) [2,8..]

  where putchar ((charp, color), column) = mdo
          load tmp1 column
          load tmp2 color
          load tmp3 charp
          drawChar tmp1 rw tmp3 tmp2 gb
        rs@[tmp1,tmp2,tmp3] = take 3 $ getFree [gb, rw]

printf :: GameState -> TAsm ()
printf state = using rs $ mdo
  load gb (frameBuffer state) <| indirect
  load rowReg 1
  f <- asSubrutine $ drawChar columnReg rowReg charpReg colorReg gb
  mapM_ (putchar f) $ zip3 chars colors [2,8..]

  where rs@[colorReg, columnReg, rowReg, charpReg, gb, fbase] = take 6 $ getFree []
        chars = (gameString state)
        colors = gameStringColor state

        putchar f (charp, color, column) = mdo
          load charpReg charp <| indirect
          load colorReg color <| indirect
          load columnReg column

          jsr f


printfMid :: GameState -> TAsm ()
printfMid state = using rs $ mdo
  load gb (frameBuffer state) <| indirect
  load rowReg 57
  f <- asSubrutine $ drawChar columnReg rowReg charpReg colorReg gb
  mapM_ (putchar f) $ zip3 chars colors [2,8..]

  where rs@[colorReg, columnReg, rowReg, charpReg, gb, fbase] = take 6 $ getFree []
        chars = (gameMidString state)
        colors = gameMidStringColor state

        putchar f (charp, color, column) = mdo
          load charpReg charp <| indirect
          load colorReg color <| indirect
          load columnReg column

          jsr f
