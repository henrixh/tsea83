-- | In this file the language facing the assembly programmer is defined
-- using the datatypes and instances defined in TAsmMonad.

{-# LANGUAGE RecursiveDo #-}

module TAsm (
  TAsm
-- *Addressing modes
  , constant
  , immediate
  , indirect
  , indexed
  , (<|)
-- * Define assembly operations
-- ** Special operations
  , nop
  , halt
  , glob
-- ** Flow Control
  , jmp
  , jnz
  , jz
  , jeq
  , jsr
  , rts
  , rti
-- ** Flags
  , sflag
  , lflag
-- ** Stack operations
  , push
  , pop
  , ssp
  , lsp
-- ** Loading and storing
  , load
  , store
-- ** Arithmetic operations
  , add
  , sub
  , mul
  , mulu
-- **Shifts and rotates
  , lsl
  , lsr
  , asr
  , asl
  , lrr
  , lrl
-- ** Binary Logic
  , and'
  , or'
  , not'
  , xor'
-- ** Data transport
  , cp
  , swap
  , clr
-- * Reexports
-- ** Registers
  , gr0
  , gr1
  , gr2
  , gr3
  , gr4
  , gr5
  , gr6
  , gr7
  , gr8
  , gr9
  , gr10
  , gr11
  , gr12
  , gr13
  , gr14
  , gr15
-- ** Label
  , TAsm.label
  , dataRow
-- ** Globals
  , sevenSeg
  , gpuBase
  , interuptVector
  , interuptEnable
  , timer1
  , timer2
  , timer3
  , timer4
  , sound4
  , sound1
  , sound2
  , sound3
  ) where

import Data.Word
import TAsmMonad
import Control.Monad

-- Helper functions
fromOp :: Operand -> TAsm ()
fromOp op = row $ InstrR $ nopInstr {
              operand = op
            }

fromOpReg :: Operand -> Reg -> TAsm ()
fromOpReg op reg = row $ InstrR $ nopInstr {
                     operand = op
                   , gra = reg
                   }
fromOpRegReg :: Operand -> Reg -> Reg -> TAsm ()
fromOpRegReg op r1 r2 = row $ InstrR $ nopInstr {
                          operand = op
                        , gra = r1
                        , grb = r2
                        }

-- Helper function for instructions on the form "OP GRa GRb"
binOp :: Operand -> Reg -> Reg -> TAsm ()
binOp = fromOpRegReg

-- Helper function for rotates and shifts
fromOpWord4 :: Operand -> Reg -> Word8 -> TAsm ()
fromOpWord4 op r w = mdo
  fromOpReg op r
  dataRow $ fromIntegral w

-- Helper functions for data and adresses
dataRow :: Word16 -> TAsm ()
dataRow d = row $ DataR d

adrRow :: Position -> TAsm ()
adrRow a = row $ DataR a

-- Convenience function to reduce boilerplate. Simply adds an
-- address row after it's argument.
withAdr :: TAsm () -> Position -> TAsm ()
withAdr instr p = instr >> adrRow p

-- |Infix operator to add an addressing mode to an instruction.
-- now the assembly programmer can write stuff like
-- @
-- store GR3 0x0478 <| indirect
-- @
--
-- It is important to note that the usage of this operator is NOT
-- limited to places where it should be used.
-- @
-- do
--   jmp 0x5000
--   add gr1 gr2 <| immediate
-- @
-- The above example will fail to run.
(<|) :: TAsm a -> Mode -> TAsm a
a <| m = a >>= \r ->
         setAMod m >>= \_ ->
         return r

-- |Constant addressing mode, the given data/address will be used directly
constant = Dir

-- |In need of strict definition
immediate = Im

-- |In need of strict definition
indirect = In

-- |In need of strict definition
indexed = Ix



-- BEGIN DECLARATION OF ASSEMBLY OPERANDS

-- RANDOM STUFF


-- |No operation
nop = fromOp NOP

-- |Halt the machine
halt = fromOp HALT

-- |Set a global register
glob g = fromOpRegReg GLOB (globToReg g)




-- FLOW CONTROL

-- |Unconditional jump to the address
jmp = withAdr $ fromOp JMP

-- |Jump if Z=0
jnz = withAdr $ fromOp JNZ

-- |Jump ip Z=1
jz = withAdr $ fromOp JZ

-- |Jump if GRa == GRb
jeq r1 r2 = withAdr $ fromOpRegReg JEQ r1 r2

-- |Unconditional jump to subroutine
jsr = withAdr $ fromOp JSR

-- |Return from subroutine
rts = fromOp RTS

-- |Return from interrupt
rti = fromOp RTI




-- FLAG OPERANDS

-- |Store flags to GRx
sflag = fromOpReg SFLAG

-- |Load flags from GRx
lflag = fromOpReg LFLAG



-- STACK OPERANDS

-- |Push GRx to the stack
push = fromOpReg PUSH

-- |Pop stack to GRx
pop = fromOpReg POP

-- |Store stack pointer in GRx
ssp = fromOpReg SSP

-- |Load stack pointer from GRx
lsp = fromOpReg LSP



-- LOADING AND STORING

-- |Load data to GRx from address
load = withAdr . fromOpReg LOAD

-- |Store data from GRx on address
store = withAdr . fromOpReg STORE



-- ARITHMETIC OPERANDS

-- |Add two registers
add = binOp ADD

-- |Subtract two registers
sub = binOp SUB

-- |Multiply two registers
mul = binOp MUL

-- |Unsigned multiply of two registers
mulu = binOp MULU



-- ROTATES AND SHIFTS

-- |Logical Shift Left
lsl = withAdr . fromOpReg LSL

-- |Logical Shift Right
lsr = withAdr . fromOpReg LSR

-- |Arithmetic Shift Right
asr = withAdr . fromOpReg ASR

-- |Arithmetic Shift Left
asl = withAdr . fromOpReg ASL

-- |Logical Rotate Right
lrr = withAdr . fromOpReg LRR

-- |Logical Rotate Left
lrl = withAdr . fromOpReg LRL



-- LOGICAL OPERANDS

-- |Bitwise And. Please take special notice of the added "'" to
-- differentiate from haskells built-ins.
and' = binOp AND

-- |Bitwise Or
or' = binOp OR

-- |Bitwise Not
not' = fromOpRegReg NOT

-- |Bitwise Xor
xor' = binOp XOR

-- |Copy data from GRa to GRb
cp = binOp CP

-- |Swap the contents of the registers
swap = binOp SWAP

-- |Set all bits in GRx to zero
clr = fromOpReg CLR


-- REEXPORTS

-- REGISTERS
gr0  = GR0
gr1  = GR1
gr2  = GR2
gr3  = GR3
gr4  = GR4
gr5  = GR5
gr6  = GR6
gr7  = GR7
gr8  = GR8
gr9  = GR9
gr10 = GR10
gr11 = GR11
gr12 = GR12
gr13 = GR13
gr14 = GR14
gr15 = GR15

-- Globals
sevenSeg = Glob0
gpuBase  = Glob1
interuptVector = Glob3
interuptEnable = Glob4
sound4   = Glob8
sound1   = Glob9
sound2   = Glob10
sound3   = Glob11
timer1   = Glob12
timer2   = Glob13
timer3   = Glob14
timer4   = Glob15
label = TAsmMonad.label
