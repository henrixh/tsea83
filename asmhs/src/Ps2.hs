{-# LANGUAGE RecursiveDo #-}
module Ps2 where
import TAsm
import TAsmMonad hiding (label)
import Control.Monad
import Constants
import Font
import Board
import Helpers
import GameState


ps2InteruptRoutine :: GameState
                      -> TAsm()
ps2InteruptRoutine state = using rs $ mdo
  scrollMemory state gr15
  --Reset Game with ESC key
  load reg1 c_keyEsc
  jeq reg1 gr15 reset
  jmp updatePause

  reset <- label
  resetGame state

  --Update paused state
  updatePause <- label
  updatePausedState state gr15

  --Update speed
  updateGameSpeed state gr15

  --Update song
  changeSong state gr15

  --Check if game is paused
  load reg1 (paused state) <| indirect
  or' reg1 reg1
  jnz exit

  --Move players
  mapM_ (\x -> movePlayer x gr15) (players state)

  exit <- label

  return ()
  where rs@[reg1] = take 1 $ getFree []


scrollMemory :: GameState -> Reg -> TAsm()
scrollMemory state key= using rs $ mdo
  jmp begin
  base <- label
  dataRow 0

  --Compare keys
  begin <- label
  load reg1 c_keyPageUp
  jeq reg1 key pageUp
  load reg1 c_keyPageDown
  jeq reg1 key pageDown
  jmp exit

  --Page Up
  pageUp <- label
  load reg1 (160*30)
  load reg2 base <| indirect
  add reg2 reg1
  store reg2 base
  jmp setBase

  --Page Down
  pageDown <- label
  load reg1 (160*30)
  load reg2 base <| indirect
  sub reg2 reg1
  store reg2 base
  jmp setBase

  --Set gpu base
  setBase <- label
  load reg1 (frameBuffer state) <| indirect
  add reg2 reg1
  glob gpuBase reg2

  exit <-label
  return ()

  where rs@[reg1, reg2] = take 2 $ getFree []

changeSong :: GameState -> Reg -> TAsm()
changeSong state key= using rs $ mdo

  ------------------
  -- Compare Keys --
  ------------------
  load reg1 c_keyF1
  jeq reg1 key f1
  load reg1 c_keyF2
  jeq reg1 key f2
  load reg1 c_keyF3
  jeq reg1 key f3
  load reg1 c_keyF4
  jeq reg1 key f4
  load reg1 c_keyF5
  jeq reg1 key f5
  load reg1 c_keyF6
  jeq reg1 key f6
  load reg1 c_keyF7
  jeq reg1 key f7
  load reg1 c_keyF8
  jeq reg1 key f8
  load reg1 c_keyF9
  jeq reg1 key f9
  load reg1 c_keyF10
  jeq reg1 key f10
  load reg1 c_keyF11
  jeq reg1 key f11
  load reg1 c_keyF12
  jeq reg1 key f12
  jmp exit

  --Function that updates the song
  let go p = mdo
        pos <- label
        load reg1 p
        store reg1 (song state)
        jmp resetPos
        return pos

  --------------
  -- Set song --
  --------------
  f1 <- go (songArray state !! 0)
  f2 <- go (songArray state !! 1)
  f3 <- go (songArray state !! 2)
  f4 <- go (songArray state !! 3)
  f5 <- go (songArray state !! 4)
  f6 <- go (songArray state !! 5)
  f7 <- go (songArray state !! 6)
  f8 <- go (songArray state !! 7)
  f9 <- go (songArray state !! 8)
  f10 <- go (songArray state !! 9)
  f11 <- go (songArray state !! 10)
  f12 <- go (songArray state !! 11)

  -- Reset song position
  resetPos <- label
  load reg1 (song state) <| indirect
  store reg1 (songPosition state)

  -- Start first note
  load reg1 13
  glob timer2 reg1

  exit <- label

  return ()
  where rs@[reg1, reg2] = take 2 $ getFree []


updatePausedState :: GameState -> Reg -> TAsm()
updatePausedState state key= using rs $ mdo

  -- Check if space was pressed
  load reg1 c_keySpace
  jeq key reg1 checkGameOver
  jmp exit

  -- Check if we want to reset the board
  checkGameOver <- label
  load reg1 (gameOver state) <| indirect
  or' reg1 reg1
  jz togglePause


  -----------------
  -- Reset round --
  -----------------

  --Disable interrupts
  load reg1 0x0000
  glob interuptEnable reg1

  --Reset board
  resetGameState state
  setupBoard state

  --Reset victory string
  load reg1 (c_space + fb)
  store reg1 ((gameMidString state) !! 9)
  store reg1 ((gameMidString state) !! 10)
  store reg1 ((gameMidString state) !! 11)
  store reg1 ((gameMidString state) !! 12)
  store reg1 ((gameMidString state) !! 13)
  store reg1 ((gameMidString state) !! 14)
  store reg1 ((gameMidString state) !! 15)

  --Enable interrupts and reset game over flag
  load reg1 0x0000
  store reg1 (gameOver state)
  load reg1 0xFFFF
  glob interuptEnable reg1


  -- Toggle Pause
  togglePause <- label
  load reg1 (paused state) <| indirect
  not' reg1 reg1
  store reg1 (paused state)

  jmp exit

  exit <- label

  return ()
  where rs@[reg1] = take 1 $ getFree []
        fb = fontBase state


updateGameSpeed :: GameState -> Reg -> TAsm()
updateGameSpeed state key= using rs $ mdo
  --Load mask
  load reg2 0x0003

  ------------------
  -- Compare Keys --
  ------------------
  load reg1 c_keyPlus
  jeq reg1 key plus
  load reg1 c_keyMinus
  jeq reg1 key minus
  jmp exit

  -----------------------
  -- Update Game Speed --
  -----------------------
  plus <- label
  load reg1 (gameSpeed state) <| indirect
  inc reg1
  and' reg1 reg2
  store reg1 (gameSpeed state)
  drawSpeedBar state
  jmp exit

  minus <- label
  load reg1 (gameSpeed state) <| indirect
  dec reg1
  and' reg1 reg2
  store reg1 (gameSpeed state)
  drawSpeedBar state
  jmp exit

  -----------------------------------
  -- Load Interrupt timer and exit --
  -----------------------------------
  exit <- label
  load reg1 (gameSpeed state) <| indirect
  load reg2 (gameSpeedArray state)
  add reg2 reg1
  add reg2 reg1
  load' reg1 reg2
  glob timer1 reg1

  return ()
  where rs@[reg1, reg2] = take 2 $ getFree []


movePlayer :: Player
              -> Reg
              ->TAsm()
movePlayer player key  = using rs $ mdo

  load dir (playerDir player) <| indirect
  ------------------
  -- Compare Keys --
  ------------------
  load reg1 (keyUp $ playerKeys player)
  jeq key reg1 up
  load reg1 (keyDown $ playerKeys player)
  jeq key reg1 down
  load reg1 (keyLeft $ playerKeys player)
  jeq key reg1 left
  load reg1 (keyRight $ playerKeys player)
  jeq key reg1 right
  jmp exit


  ----------------------
  -- Update Direction --
  ----------------------
  up <- label
  load dir c_dirUp
  jmp exit

  down <- label
  load dir c_dirDown
  jmp exit

  left <- label
  load dir c_dirLeft
  jmp exit

  right <- label
  load dir c_dirRight
  jmp exit

  -- Exit
  exit <- label
  store dir (playerDir player)

  return ()
  where rs@[dir, reg1] = take 2 $ getFree []
