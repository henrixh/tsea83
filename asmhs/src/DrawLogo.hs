{-# LANGUAGE RecursiveDo #-}
module DrawLogo where

import TAsm
import TAsmMonad hiding (label)
import Data.Word
import GameState
import Font
import FontTools
import Constants

import Control.Monad
import Helpers
import Board
import Logo

drawLogo :: GameState -> TAsm ()
drawLogo state = using rs $ mdo
  jmp begin

  l <- label
  logo

  begin <- label

  load color c_logoBG
  load gb (frameBuffer state) <| indirect
  drawBlob 0 120 color gb

  load color c_logoFG
  load column 0xFFFF -- Dunno why, it works.
  load rw 0
  load charp l

  drawChar column rw charp color gb

  where rs@[color, gb, column, rw, charp] = take 5 $ getFree []
