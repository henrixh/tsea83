-- |This module a simple monad aimed at defining an EDSL for assembly
-- programming. Note the MonadFix instance, which is needed to
-- provide the possibility of using labels before their declaration.

module MonadAsm where
import Control.Monad.Fix
import Control.Applicative (Applicative(..))
import Control.Monad (liftM, ap)

-- |The state of the monad
type AsmState r pos = ([r], pos)
-- |The monadic datatype
newtype Asm r pos a = Asm {
        runAsm :: AsmState r pos -> (a, AsmState r pos)
        }

-- |Create a list of rows from an initial state
asm :: AsmState r pos -> Asm r pos a -> [r]
asm initialState (Asm f) = d
    where
        (_,(d,_)) = f initialState

-- |Monadic return
returnAsm :: a -> Asm r pos a
returnAsm a = Asm $ \s -> (a, s)

-- |Monadic bind
bindAsm :: Asm r pos a -> (a -> Asm r pos b) -> Asm r pos b
bindAsm m k = Asm $ \s -> let (a,s') = runAsm m s
                         in runAsm (k a) s'

-- |Declare the Monad instance
instance Monad (Asm r pos) where
         (>>=) = bindAsm
         return = returnAsm

-- |Create the MonadFix instance to enable the use of labels before
-- their declaration.
instance MonadFix (Asm r pos) where
         mfix f = Asm $ \s -> let (a, s') = runAsm (f a) s
                             in (a, s')
instance Functor (Asm r pos) where
    fmap = liftM

instance Applicative (Asm r pos) where
    pure  = return
    (<*>) = ap
