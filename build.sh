#!/bin/sh
if [ "$#" -ne 1 ] && [ "$#" -ne 2 ]; then
  echo "Wrong number of arguments."
  echo "Usage ./build.sh username [host in mux]"
  echo "Pleb."
  exit 1
fi

clear
echo "---------------- BUILDING (m)ASSEMBLY ------------------"
echo "Running assembly build..."
cd microcode
python2 converter.py > /dev/null\
    || { echo "Microcode assembly converter failed"; exit 1; }
cd ..

mv microcode/microcode.bin .
mv microcode/madr_constants.vhd src/

cd asmhs/src/
runhaskell Main.hs > assembly.bin \
    || { echo "Assembly generation failed"; cd ../..; exit 1;}
mv assembly.bin ../..
cd ../..

runhaskell buildtools/createVHD.hs > src/random_access_memories.vhd \
    || { echo "Random Access Memories failed to build" ; exit 1;}

echo "Assembly building successful."


echo ""
echo "---------------- TRANSFERING SOURCE ------------------"
echo "Removing previous directory..."
ssh -t $1@ixtab.edu.isy.liu.se "rm -rf tsea83_build" > /dev/null
echo "Previous files are gone"
echo "Transfering files..."
scp -r src $1@ixtab.edu.isy.liu.se:tsea83_build > /dev/null \
    || { echo "Transfer failed!"; exit 1;}
echo "Transfer complete"


if [ "$#" -ne 2 ]; then
    echo "No host given. Using ixtab. FPGA programming will fail."
    echo "Trying to connect to ixtab..."
    ssh -t $1@ixtab.edu.isy.liu.se \
    "module add TSEA83; cd tsea83_build; sh build.sh" \
    || { echo "Something went to hell on ixtab..."; exit 1;}
    exit 0
fi

echo -n ''
echo "Connecting to $2 thru ixtab as $1..."
ssh -t $1@ixtab.edu.isy.liu.se \
    "ssh -t $1@$2 \"module add TSEA83; cd tsea83_build; sh build.sh\"" \
    || { echo "Something went to hell on $2..."; exit 1;}

echo "K THX BYE."
