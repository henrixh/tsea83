\chapter{Hårdvara}
\section{Översikt}
Datorns arkitektur är gjort för att vara så generell som möjligt. All spellogik
och grafik ligger i assemblykod, inte i hårdvara. Man skulle även köra till
exempel tetris på samma dator med oförändrad hårdvara, så länge man skrev
assemblykoden. Se figur \ref{figure:overview} för en översikt.

\begin{figure}
  \includegraphics[width=\textwidth]{Hognivaskiss.pdf}
  \caption{Översikt av datorn}
  \label{figure:overview}
\end{figure}

\section{CPU}
Vår dators processor bygger vidare på den ``Björn Lindskogdator'' som används
som exempel under föreläsningar och labbar i kursen. Processorn har 16 stycken
generella register och en stackpekare synliga för
assemblyprogrammeraren. Dessutom har assemblyprogrammeraren tillgång till 16
stycken kontrollregister för att styra andra delar av datorn, till exempel
ljudets frekvens. Figur \ref{figure:databuss} visar hur registren är
ihopkopplade. Figur \ref{figure:cpu_controller} visar hur processorns
kontrollenhet är utformad.

\begin{figure}
  \includegraphics[width=\textwidth]{databuss.pdf}
  \caption{Schema över databussen. Alla bussar är 16 bitar breda.}
  \label{figure:databuss}
\end{figure}


\begin{figure}
  \includegraphics[angle=270,width=\textwidth]{cpu_controller.pdf}
  \caption{Schema över kontrollenhet}
  \label{figure:cpu_controller}
\end{figure}

\subsection{Register}
På microprogrammerarens nivå visar sig ytterligare register och signaler

\paragraph{IR \texttt{(rw)}} Instruktionsregister avsett för lagring av den instruktion som just nu körs. \\

\paragraph{PM \texttt{(rw)}} Representerar minnet. Man läser/skriver med
avseende på den adress som finns i \texttt{ASR}.

\paragraph{PC \texttt{(rw, pc++)}} Ett register för att hålla koll var i minnet
man är och kör kod. Kan även inkrementeras till nästa rad i minnet.

\paragraph{ASR \texttt{(wo)}} Den data som läses från \texttt{PM} kommer från
den rad i minnet som detta register pekar på. Notera att registret är write
only.

\paragraph{SP \texttt{(rw, sp++, sp--)}} Processorns stackpekare. Denna kan
användas av assemblyprogrammeraren genom \texttt{push} och \texttt{pop}, men
används också för att stöda interupts och subrutiner.

\paragraph{AR \texttt{(ro)}} Ackumulatorregister för processorns ALU. Man kan
inte skriva direkt till \texttt{AR}, utan man måste gå via ALU.

\paragraph{HR \texttt{(rw)}} Ett hjälpregister för att underlätta
microprogrammerarens arbete.

\paragraph{GRA \texttt{(rw)}} Det generella register som bit 7 till 4 i
\texttt{IR} refererar till.

\paragraph{GRB \texttt{(rw)}} Det generella register som bit 3 till 0 i
\texttt{IR} refererar till.

\paragraph{MCM \texttt{(ro)}} Microkodsfält för konstanter som kan läsas till
bussen.

\paragraph{ITR \texttt{(ro)}} Ett register för datan som en interrupt
skickar med.

\paragraph{IADR \texttt{(ro)}} Ett register som innehåller adressen
till den nuvarande interrupt rutinen.

\paragraph{FLG \texttt{(rw)}} Representerar flaggor i processorn. Används för
att spara undan och återställa flaggor vid interrupt.
\end{tabular}

\subsubsection{Globala kontrollregister  \texttt{(wo)}}
Datorn har 16 globala kontrollregister som kan sättas av assemblyprogrammeraren
för att styra diverse funktioner i datorn. Vilket kontrollregister som en
instruktion refererar till bestämms av bit 7 till 4 i \texttt{IR}. Se tabell
\ref{table_glob} för en lista över kontrollregistrens funktioner.

\begin{table}
  \centering
  \caption{De globala kontrollregistrens funktioner}
  \label{table_glob}
\begin{tabular}{|c|c|}
 \hline
 \# & Beskrivning \\
 \hline
 0 & Data till sjusegmentsdisplay \\
 1 & Den minnesadress GPUn ska börja rita från \\
 2 & Ledig \\
 3 & Adress till interruptvektorn \\
 4 & Slå på och stäng av interrupts \\
 5 & Ledig \\
 6 & Ledig \\
 7 & Ledig \\
 \hline
 8 & Styrning av röst 4 \\
 9 & Styrning av röst 1 \\
 10 & Styrning av röst 2 \\
 11 & Styrning av röst 3 \\
 12 & Tid till nästa timerinterrupt från timer 1 i millisekunder \\
 13 & Tid till nästa timerinterrupt från timer 2 i millisekunder \\
 14 & Tid till nästa timerinterrupt från timer 3 i millisekunder \\
 15 & Tid till nästa timerinterrupt från timer 4 i millisekunder \\
 \hline
\end{tabular}
\end{table}

\subsection{ALU}
Datorns ALU har stöd för de flesta aritmetiska och logiska operationerna på 16
bitars heltal. Vissa funktioner är dock inte fullständigt implementerade. Till
exempel saknas stöd för overflow och carryflaggorna, även om de finns med i
koden. ALUn är skriven som en VHDL funktion för att enkelt kunna bryta ut den ur
CPU-koden. Den styrs med 5 bitar, dock används bara 17 av de tillgängliga
instruktionerna. Figur \ref{figure:alu} visar ett grovt blockshema över datorns
ALU.

\begin{figure}
  \centering
  \includegraphics[width=5cm]{alu.pdf}
  \caption{Blockschema över datorns ALU. Alla bussar är 16 bitar.}
  \label{figure:alu}
\end{figure}


\subsection{Instruktionsformat}
Det instruktionsformat som datorn använder visas i figur
\ref{table:instrformat}. Då datorn har 16 register har vi valt att förändra
instruktionsformatet drastiskt från ``Björn Lindskog''-datorn. I datorn sker de
flesta operationer med avseende på två register istället för ett register och
primärminnet. Instruktionerna saknar därför adressfält. Eventuella adress- och
datafält placeras instället för på raden under.

\begin{table}
  \centering
  \caption{Datorns instruktionsformat}
  \label{table:instrformat}
\begin{tabular}{|c|c|c|c|}
 \hline
 OP-kod & Adresseringsmod & \texttt{gra} & \texttt{grb} \\
 \hline
 6 bitar & 2 bitar & 4 bitar & 4 bitar\\
 \hline
\end{tabular}
\end{table}

\subsection{Mikrokodsinstruktionsformat}
Det mikrokodsinstruktionsformat som datorn använder sig av visas i figur \ref{table:microinstr}. 

\begin{table}
  \centering
  \caption{Datorns mikrokodsinstruktionsformat}
  \label{table:microinstr}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
 \hline
 TB & FB &ALU & SEQ & PC++ & - & LC & SP & uADR \\
 \hline
 4 bitar & 4 bitar & 5 bitar & 4 bitar & 1 bit & 1 bit & 2 bitar & 2 bitar & 9 bitar \\
 \hline
\end{tabular}
\end{table}



\section{GPU}
Datorns GPU har till uppgift att kopiera bytes från rätt plats i minnet till
VGA-utgången. Därmed blir konstruktionen väldigt enkel. Den enda styrsignal som
GPUn får är en 16-bitars adress för att tala om var i minnet bilden som ska
ritas ut börjar.

GPUn har en räknare för vilken småpixel som ritas ut, varefter man
genom en bitshift får reda på var i minnet storpixeln ligger.
Eftersom minnesläsningen ger ett 16-bitars ord väljs sedan rätt byte
ut och skickas vidare till VGA-utgången. Se figur \ref{figure:gpu}

\begin{figure}
  \includegraphics[width=\textwidth]{GPU.pdf}
  \caption{Schema över GPUn}
  \label{figure:gpu}
\end{figure}

\section{PS/2}
PS/2 kontrollerns uppgift är att läsa data från ett tangetbord och
skicka interrupts till interrupt hanteraren. Den kan alltså inte
skicka någon data till tangentbordet, så funktioner som att tända caps
lock lampan stödjs inte.

För att läsa in data så letar kontrollern efter fallande flanker på
tangebordets klocka och skickar en laddpuls varje gång den hittar
en. Denna laddpuls går till ett skiftregister som skiftar in data från
tangetbordets data signal och räknar upp en räknare som räknar
hur många bitar vi har läst. När vi har läst in alla 11 bitar som
tangebordet skickar så kollar vi den udda parity biten med ett
k-nät. Om antalet ettor är udda så ska parity biten vara 0 annars gör
vi ingenting med all data.

De scan codes som kontrollern har stöd för är extended 0
(\texttt{E0}), key up (\texttt{F0}) och de vanliga som bara skickar en
byte. Knappdatan sätts alltid till de 8 minst siknifikanta bitarna i
ordet, om det är en key up event sätts den mest signifikanta biten hög
och om det är en extended så sätts den näst mest signifikanta biten
hög. Alla andra bitar är alltid låga. Vi har alltså inte stöd för lite
mer udda tangenter så som pause knappen som skickar en extendend 1. Se
figur \ref{figure:ps2} för ett blockschema.

\begin{figure}
  \includegraphics[width=\textwidth]{PS2_Kontroller.pdf}
  \caption{Schema över  PS/2 kontrollern}
  \label{figure:ps2}
\end{figure}

\section{Minne}
Datorn använder de 64KiB blockram som finns tillgängligt till fullo. Minnet
består av $2^{15}$ ord á 16-bitar. Därmed hamnar till exempel microminne i
distribuerad ram. Vi hade problem med att få minnet att syntas korrekt av
oförklarliga anledningar. Bland annat trodde synthverktyget att det fanns
oklockade läsningar från minnet. I koden används därför ett primärminne som är
baserat på en mall från internet. \cite{stuletminne}

Observera att CPU och GPU använder olika läs/skrivportar. CPU har en
läs/skriv-port, medan GPU enbart har en läsport. Man utnyttjar därmed
möjligheten till äkta tvåportsminne som Spartan 6 kortet erbjuder.

\section{Timers}
Datorn har 4 olika timers. Varje timer läser in ett värde från ett
kontrollregister och skickar sedan en interrupt varje gång timern har
väntat det värdet i millesekunder. Den fungerar genom att den har två
räknare en som räknar klock-cykler och en andra som räknar upp varje
gång den första når 100k, varav den första då nollställs. När sedan
den andra räknaren når samma värde som kontrollregistret så skickas en
interrupt och allting nollställs. Den skickar inte med någon data till
interrupten, IDR registret blir alltså bara nollor. Om
kontrollregistret bara innehåller nollor så kommer inte timern skicka
interrupts. Se figur \ref{figure:timer} för ett blockschema.

\begin{figure}
  \includegraphics[width=\textwidth]{Timer.pdf}
  \caption{Schema över en timer}
  \label{figure:timer}
\end{figure}

\section{Interrupts}
Datorn har stöd för 8 olika interrupts, varav endast 5 används just
nu, 4 timers och PS/2 kontrollern. Interrupts fungerar genom att en enhet
som vill skicka en interrupt skickar en hög signal och ett 16 bitar
data till vår interrupt-hanterare. Den skickar sedan iväg ett
interrupt till CPUn. Om flera interrupts kommer på samma klock cykel
så väljs den med lägst interrupt vektor, och om flera interrupts
kommer på samma assembly kommando så väljs den som kom först.

När en interrupt sedan kommer till CPUn så sätts adressen till
interrupt rutinen i IADR registret genom att addera interruptens vektor
med interrupt base kontroll registret. Den bestämmer alltså var alla
adresser till interrupt rutinerna ligger. Interrupt datan läggs i IDR
registret. Båda dessa register är åtkommliga i mikrokoden.

\section{Ljudkort}
Datorns ljudkort kan instrueras att spela upp en angiven ton. Ljudet
skapas via en timer som ökar i varje klockcykel, samt en maxgräns för
timern som motsvarar tonens våglängd. Om timern är större än
ljudutgångens givna pulsbredd (procentandelen av hög/låg för
våglängden), så sätts ljudutgången till hög, annars låg. Detta skapar
en fyrkantsvåg av angiven frekvens. Vilken ton som ska spelas upp
bestäms av en kontrollsignal som hämtar våglängden med hjälp av två
uppslagstabeller för alla toner mellan \texttt{C0} och
\texttt{B8}. Tabellerna innehåller information om alla toners index
samt våglängd. Tabellen för index är mestadels till för att hålla koll
på de rena toner som finns, samt spara plats i det programmerbara
minnet.
