import System.IO


main :: IO ()
main = do
     mcodeH <- openFile "microcode.bin" ReadMode
     mmem <- hGetContents mcodeH
     asmcodeH <- openFile "assembly.bin" ReadMode
     asmmem <- hGetContents asmcodeH

     let masmRows = take 512 $
                  (filter (\x -> length x == 8) $ lines mmem)
                  ++ cycle [ take 8 $ cycle ['0']]
     let tasmRows = take 32768 $ (lines asmmem)
                  ++ cycle [ take 16 $ cycle ['0']]


     putStrLn $ createVhd masmRows tasmRows

createVhd :: [String] -> [String] -> String
createVhd masm tasm = header ++ (generateM masm) ++ (generateT tasm) ++ footer

generateM xs = "constant micro_memory : m_mem_t := (\n"
               ++ go xs
    where
        go (x:[]) = "X" ++ show x ++ ");\n"
        go (x:xs) = "X" ++ show x ++ ",\n" ++ go xs

generateT xs = "constant primary_memory : word_array (0 to 32767) := (\n"
               ++ go xs
    where
        go (x:[]) = show x ++ ");\n"
        go (x:xs) = show x ++ ",\n" ++ go xs

header = unlines ["library IEEE; use IEEE.STD_LOGIC_1164.ALL;"
                 ,"use IEEE.STD_LOGIC_UNSIGNED.ALL;"
                 ,"use work.types.ALL;"
                 ,"package random_access_memories is"]

footer = unlines ["end;"
                 , "package body random_access_memories is"
                 , "end package body;"
                 ]
