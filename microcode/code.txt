# Globala register

# TB/FB
set $PM  0001
set $ASR 0010
set $PC  0011
set $SP  0100
set $AR  0101
set $HR  0110
set $GRA 0111
set $GRB 1000
set $MCM 1001
set $GLO 1010
set $IR  1011
set $ITR 1100
set $IADR 1101
set $FLG 1110

# SEQ
set $uPC_INC 0000
set $uPC_K1  0001
set $uPC_K2  0010
set $uPC_0   0011
set $JMP_Z_0 0100
set $JMP     0101
set $sub_JMP 0110
set $SUB_RTS 0111
set $JMP_Z_1 1000
set $JMP_N_1 1001
set $JMP_C_1 1010
set $JMP_O_1 1011
set $JMP_L_1 1100
set $JMP_C_0 1101
set $JMP_O_0 1110
set $HALT    1111

# ALU
set $ALU_NOP    00000
set $ALU_SET    00001
set $ALU_NOT    00010
set $ALU_RESET  00011
set $ALU_ADD    00100
set $ALU_SUB    00101
set $ALU_MUL    00110
set $ALU_MULU   00111
set $ALU_AND    01000
set $ALU_OR     01001
set $ALU_XOR    01010
set $ALU_ADD    01011
set $ALU_LSL    01100
set $ALU_LSR    01101
set $ALU_ASL    01110
set $ALU_ASR    01111
set $ALU_LRL    10000
set $ALU_LRR    10001

# PC
set $PC_NOP  0
set $PC_INC  1

# LC
set $LC_NOP  00
set $LC_DEC  01
set $LC_BUS  10
set $LC_uADR 11

# SP
set $SP_NOP  00
set $SP_INC  01
set $SP_DEC  10
set $SP_DEAD 11


# Functions
! X->X     | TB, FB
! End      | SEQ:=$uPC_0
! setALU X | TB, ALU:=$ALU_SET

# Example of flags
# SEQ := $JMP_O_0 ; uADR := @111
# flag @111

--- Startup
$PC -> $ASR
R := 0
$PM -> $IR ; P := $PC_INC
SEQ := $uPC_K2

--- m_IMMEDIATE
$PC -> $ASR
P := $PC_INC ; SEQ := $uPC_K1

--- m_INDIRECT
$PC -> $ASR
R := 0
$PM -> $ASR
SEQ := $uPC_K1

--- m_CONSTANT
$PC -> $ASR
SEQ := $uPC_K1

--- m_INDEXED
#$PC -> $ASR
#R := 0
#TB := $IR ; ALU := $ALU_SET
#TB := $GRA ; ALU := $ALU_ADD
#$AR -> $ASR
SEQ := $uPC_K1


--- m_intr
$SP -> $ASR
R := 0
SP := $SP_INC
$PC -> $PM
$SP -> $ASR
R := 0
SP := $SP_INC
$FLG -> $PM
uADR := 111111111; $MCM-> $IR
R := 0
$SP -> $ASR
R := 0
SP := $SP_INC
$GRB -> $PM
$ITR -> $GRB
$IADR -> $ASR
R := 0
$PM -> $PC
#uADR := 000000000
#$MCM -> $IR
#$IADR -> $GLO
#SEQ := $HALT
End

--- m_RTI
uADR := 111111111; $MCM -> $IR
R := 0
SP := $SP_DEC
$SP -> $ASR
R := 0
SP := $SP_DEC
$PM -> $GRB
$SP -> $ASR
R := 0
SP := $SP_DEC
$PM -> $FLG
$SP -> $ASR
R := 0
$PM -> $PC
End

--- m_NOP
End

--- m_HALT
SEQ := $HALT
End

--- m_GLOB
$GRB -> $GLO ; End

--- m_JMP
$PM -> $PC ; End
--- m_JNZ
SEQ := $JMP_Z_1 ; uADR := @jnz_skip
$PM -> $PC ; End
flag @jnz_skip
P := $PC_INC ; End

--- m_JZ
SEQ := $JMP_Z_0 ; uADR := @jz_skip
$PM -> $PC ; End
flag @jz_skip
P := $PC_INC ; End

--- m_JEQ
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_SUB
SEQ := $JMP_Z_0 ; uADR := @jeq_skip
$PM -> $PC ; End
flag @jeq_skip
P := $PC_INC ; End

--- m_JNE
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_SUB
SEQ := $JMP_Z_1 ; uADR := @jne_skip
$PM -> $PC ; End
flag @jne_skip
P := $PC_INC ; End

--- m_JSR
P := $PC_INC
R := 0
$PM -> $HR
$SP -> $ASR
SP := $SP_INC
$PC -> $PM
$HR -> $PC
End

--- m_RTS
SP := $SP_DEC
R := 0
$SP -> $ASR
R := 0
$PM -> $PC
End

--- m_SFLAG
End

--- m_LFLAG
End

--- m_PUSH
$SP -> $ASR
SP := $SP_INC
$GRA -> $PM
End

--- m_POP
SP := $SP_DEC
R := 0
$SP -> $ASR
R := 0
$PM -> $GRA
End

--- m_SSP
$GRA -> $SP
End

--- m_LSP
$SP -> $GRA
End

--- m_LOAD
$PM -> $GRA ; P := $PC_INC ; End

--- m_STORE
$PM -> $ASR
R := 0
$GRA -> $PM ; P := $PC_INC
End

--- m_ADD
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_ADD
$AR -> $GRA ; End

--- m_SUB
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_SUB
$AR -> $GRA ; End

--- m_MUL
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_MUL
$AR -> $GRA ; End

--- m_MULU
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_MULU
$AR -> $GRA ; End

--- m_LSL
TB := $GRA ; ALU := $ALU_SET
TB := $PM ; P := $PC_INC ; LC := $LC_BUS
R := 0
flag @lsl_loop
LC := $LC_DEC ; SEQ := $JMP_L_1 ; uADR := @lsl_done
ALU := $ALU_LSL ; SEQ := $JMP ; uADR := @lsl_loop
flag @lsl_done
$AR -> $GRA ; End

--- m_LSR
TB := $GRA ; ALU := $ALU_SET
TB := $PM ; P := $PC_INC ; LC := $LC_BUS
R := 0
flag @lsr_loop
LC := $LC_DEC ; SEQ := $JMP_L_1 ; uADR := @lsr_done
ALU := $ALU_LSR ; SEQ := $JMP ; uADR := @lsr_loop
flag @lsr_done
$AR -> $GRA ; End

--- m_ASR
TB := $GRA ; ALU := $ALU_SET
TB := $PM ; P := $PC_INC ; LC := $LC_BUS
R := 0
flag @asr_loop
LC := $LC_DEC ; SEQ := $JMP_L_1 ; uADR := @asr_done
ALU := $ALU_ASR ; SEQ := $JMP ; uADR := @asr_loop
flag @asr_done
$AR -> $GRA ; End

--- m_ASL
TB := $GRA ; ALU := $ALU_SET
TB := $PM ; P := $PC_INC ; LC := $LC_BUS
R := 0
flag @asl_loop
LC := $LC_DEC ; SEQ := $JMP_L_1 ; uADR := @asl_done
ALU := $ALU_ASL ; SEQ := $JMP ; uADR := @asl_loop
flag @asl_done
$AR -> $GRA ; End

--- m_LRR
TB := $GRA ; ALU := $ALU_SET
TB := $PM ; P := $PC_INC ; LC := $LC_BUS
R := 0
flag @lrr_loop
LC := $LC_DEC ; SEQ := $JMP_L_1 ; uADR := @lrr_done
ALU := $ALU_LRR ; SEQ := $JMP ; uADR := @lrr_loop
flag @lrr_done
$AR -> $GRA ; End

--- m_LRL
TB := $GRA ; ALU := $ALU_SET
TB := $PM ; P := $PC_INC ; LC := $LC_BUS
R := 0
flag @lrl_loop
LC := $LC_DEC ; SEQ := $JMP_L_1 ; uADR := @lrl_done
ALU := $ALU_LRL ; SEQ := $JMP ; uADR := @lrl_loop
flag @lrl_done
$AR -> $GRA ; End

--- m_AND
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_AND
$AR -> $GRA ; End

--- m_OR
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_OR
$AR -> $GRA ; End

--- m_NOT
TB := $GRB ; ALU := $ALU_NOT
$AR -> $GRA ; End

--- m_XOR
TB := $GRA ; ALU := $ALU_SET
TB := $GRB ; ALU := $ALU_XOR
$AR -> $GRA ; End

--- m_CP
$GRB -> $GRA ; End

--- m_SWAP
$GRB -> $HR
$GRA -> $GRB
$HR -> $GRA ; End

--- m_CLR
ALU := $ALU_RESET
$AR -> $GRA ; End
