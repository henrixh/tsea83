import codecs
import sys
from numberBase import *

RES = "\033[0m"
RED = "\033[31;1m"

var = {}
func = []
lines = []
flags = {}

# Read file

filename = 'code.txt'
f = codecs.open(filename, 'r')
text = f.read().split("\n")
for line in text:
    if line:
        if line == 'Stop':
            break
        line = line.split('#')[0]
        if not line:
            continue
        if line[:3] == 'set':
            name, value = line[4:].split()
            var[name] = value
            continue
        if line[:4] == 'flag':
            name = line[5:].split()[0]
            var[name] = dec2bi(str(len(lines)))
            continue
        if line[:3] == '---':
            name = line[4:].split()[0]
            flags[name] = str(len(lines))
            continue
        if line[0] == '!':
            line = line[1:].replace(' ', '').split('|')
            cmd = line[0].replace('X', '').replace(',', '')
            param = line[0].split('X')
            if line[0].count('X'):
                param = zip(param, ['X']*(len(param)-1))
                param = [j for i in param for j in i if j]
            args = line[1].split(',')
            func.append((cmd, param, args))
            continue
        line = line.split(';')
        line = [pos.replace(' ', '') for pos in line]
        lines.append(line)
f.close()


# Let's get serious!

def blank_row():
    # TB   FB   ALU   SEQ  P R LC SP uADR
    # 0000 0000 00000 0000 0 0 00 00 000000000
    return {
        'TB': '0000',
        'FB': '0000',
        'ALU': '00000',
        'SEQ': '0000',
        'P': '0',
        'R': '0',
        'LC': '00',
        'SP': '00',
        'uADR': '000000000'
    }

def make_row(row):
    text = row['TB'] +' '+ row['FB'] +' '+ row['ALU'] +' '+ row['SEQ'] \
           +' '+ row['P'] +' '+ row['R'] + ' ' + row['LC'] +' '+ row['SP'] +' '+ row['uADR']
    #print text
    binary = row['TB'] + row['FB'] + row['ALU'] + row['SEQ'] + row['P'] + row['R'] \
             + row['LC'] + row['SP'] + row['uADR']
    try:
        return bi2hex(binary)
    except:
        sys.stderr.write( '{}Error{}: Can\'t compress row "{}" \n'.format(RED, RES, text))
        sys.exit(-1)

def write(row, name, value):
    if int(row[name]) != 0:
        sys.stderr.write( '{}Warning{}: Overwriting {} with {} \n'.format(RED, RES, name, value))
    if value in var:
        if len(row[name]) < len(var[value]):
            sys.stderr.write('{}Error{}: Wrong size variable {} with {} \n'.format(RED, RES, name, value))
            sys.exit(-1)
        while len(row[name]) > len(var[value]):
            var[value] = '0' + var[value]
        #print 'Var:', value, var[value]
        row[name] = var[value]
    else:
        if value.replace('0','').replace('1',''):
            sys.stderr.write( '{}Error{}: Variable "{}" not found \n'.format(RED, RES, value))
            sys.exit(-1)
        if len(row[name]) != len(value):
            sys.stderr.write( '{}Error{}: Wrong size {} with {} \n'.format(RED, RES, name, value))
            sys.exit(-1)
        row[name] = value


result = []
for line in lines:
    #print line
    row = blank_row()
    for cmd in line:
        found = False
        if ':=' in cmd:
            name, value = cmd.split(':=')
            write(row, name, value)
            found = True
        for f in func:
            if f[0] in cmd:
                args = []
                for arg in cmd.split(f[0]):
                    for a in arg.split(','):
                        if a:
                            args.append(a)
                if len(args) == f[1].count('X'):
                    for name in f[2]:
                        if ':=' in name:
                            name, value = name.split(':=')
                            write(row, name, value)
                        else:
                            write(row, name, args.pop(0))
                        found = True
        if not found:
            sys.stderr.write( '{}Error{}: "{}"\n'.format(RED, RES, cmd))
            sys.exit(-1)

    result.append(make_row(row))
    #print


filename = 'microcode.bin'
f = codecs.open(filename, 'w+')
for i in range(len(result)):
    l = dec2hex(i)
    n = result[i]
    l = '0'*(2-len(l)) + l
    n = '0'*(8-len(n)) + n
    #print '{}: {}'.format(l, n)
    f.write('{}'.format(n) + '\n')
f.close()

for key in flags:
    flags[key] = int(flags[key])
longest = 0
for name in sorted(flags.items(), key=lambda x: x[1]):
    if len(name[0]) > longest:
        longest = len(name[0])

filename = 'madr_constants.vhd'
f = codecs.open(filename, 'w+')
f.write('library IEEE;\n')
f.write('use IEEE.STD_LOGIC_1164.ALL;\n')
f.write('use IEEE.STD_LOGIC_UNSIGNED.ALL;\n')
f.write('\n')
f.write('package madr_constants is\n')

for name in sorted(flags.items(), key=lambda x: x[1]):
    name, l = name[0], dec2bi(str(name[1]))
    s = '0'*(9-len(l))+l
    #print s
    t = '  constant {}{}\t: std_logic_vector (8 downto 0) := "{}";\n'.format(name, ' '*(longest-len(name)), s)
    f.write(t)

f.write('end;\n')
f.write('\n')
f.write('package body madr_constants is\n')
f.write('\n')
f.write('end package body;\n')
f.close()
