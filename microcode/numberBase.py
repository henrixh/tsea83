# Useless converter

class BaseConverter(object):
    decimal_digits = '0123456789'

    def __init__(self, digits, sign='-'):
        self.sign = sign
        self.digits = digits
        if sign in self.digits:
            raise ValueError('Sign character found in converter base digits.')

    def __repr__(self):
        return "<BaseConverter: base%s (%s)>" % (len(self.digits), self.digits)

    def _convert(self, number, from_digits, to_digits):
        if str(number)[0] == self.sign:
            number = str(number)[1:]
            neg = True
        else:
            neg = False

        # make an integer out of the number
        x = 0
        for digit in str(number):
            x = x * len(from_digits) + from_digits.index(digit)

        # create the result in base 'len(to_digits)'
        if x == 0:
            res = to_digits[0]
        else:
            res = ''
            while x > 0:
                digit = x % len(to_digits)
                res = to_digits[digit] + res
                x = int(x // len(to_digits))
        return neg, res

    def encode(self, number):
        neg, value = self._convert(number, self.decimal_digits, self.digits)
        if neg:
            return self.sign + value
        return value

    def decode(self, number):
        neg, value = self._convert(number, self.digits, self.decimal_digits)
        if neg:
            return self.sign + value
        return value


base2 = BaseConverter('01')
base10 = BaseConverter('0123456789')
base16 = BaseConverter('0123456789ABCDEF')

def bi2hex(data):
    return base16.encode(base2.decode(data))

def dec2bi(data):
    return base2.encode(base10.decode(data))

def dec2hex(data):
    return base16.encode(base10.decode(data))
