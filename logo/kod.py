from PIL import Image, ImageDraw

print '{-# LANGUAGE RecursiveDo #-}'
print '-- |Main logo module'
print 'module Logo where'
print 'import TAsm'
print ' '
print '-- |Logo data'
print 'logo :: TAsm ()'
print 'logo = do'


image = Image.open("Tron.png")
WIDTH = image.size[0]
HEIGHT = image.size[1]

where = 0
for y in range(HEIGHT):
    for x in range(0, WIDTH):
        where += 1
        color = list(image.getpixel((x,y)))
        if color == [0,0,0]:
            print '  dataRow', where
            where = 0
print '  dataRow ' + str(2**16-1)

exit()


width = 160
size = 5
where = 0
row = 0
flags = {}
for k in range(len(alphabet)):
    where = 0
    s = ""
    for i in range(size+1):
        t = ""
        for j in range(size+1):
            if i == size or j == size: c = ' '
            else: c = font[size**2*k + size*i + j]
            if c == ' ':
                c = '0'
            else:
                c = 'F'
                print '                dataRow ' + str(where)
                row += 2
                where = 0
            where += 1
            t += 2*c
        where += width - size-1
        s += t
    print '                dataRow ' + str(2**16-1)
    row += 2
