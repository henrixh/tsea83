library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.types.ALL;
use work.random_access_memories.ALL;
use work.cpu_controller_constants.ALL;
use work.glob_constants.ALL;
use work.madr_constants.ALL;
use work.alu.ALL;

entity cpu is
  port(clk, rst  : in STD_LOGIC;
       cpu_read  : in word;
       intr_in   : in intr_t;
       intr_base : in word;
       intr_enabled : in std_logic;
       cpu_write : out word;
       asr       : out word;
       cpu_enable_write : out STD_LOGIC;
       glob : out word_array(15 downto 0));
end cpu;

architecture Behavioral of cpu is
  -- Registers
  signal ir : word := X"0000";
  signal pc : word := X"0000";
  signal sp : word := X"00A0";
  signal hr : word := X"0000";
  -- The general purpose registers
  signal gr : word_array(15 downto 0) := (others => X"0000");

  -- The alu data, including flags
  signal alu_d : alu_t;

  -- Interrupt
  signal interrupt : intr_t;

  ----------------------------------
  --Signals for the cpu-controller--
  ----------------------------------
  -- Type declaration for micro adresses
  subtype madr_t is std_logic_vector (8 downto 0);

  -- The micro memory
  signal m_mem : m_mem_t := micro_memory;
  signal m_row : m_row_t;
  signal mpc : madr_t := "000000000";
  signal mpc_stack : madr_t := "000000000";

  -- Loop counter and flag
  signal loop_counter : byte := X"00";
  signal l : std_logic := '0';

  -----------------------
  --Convinience Aliases--
  -----------------------

  -- Aliases for flags
  alias n is alu_d.n;
  alias c is alu_d.c;
  alias o is alu_d.o;
  alias z is alu_d.z;

  -- Instruction partition
  alias instr_op   is ir(15 downto 10);
  alias instr_amod is ir(9 downto 8);
  alias instr_gra  is ir(7 downto 4);
  alias instr_grb  is ir(3 downto 0);

  alias ar is alu_d.ar;

begin
  process (clk)
    variable temp  : word;
    variable buss  : word;
    variable k1 : madr_t;
    variable k2 : madr_t;
    variable mpc_next : madr_t;


  begin
    if rising_edge (clk) then

      -------------
      --Interrupts--
      -------------
      if interrupt.flag = '0' and intr_in.flag = '1' and intr_enabled = '1'
      then
        interrupt <= intr_in;
      end if;


      ------------------------------------------------------
      -- / __| _ \ | | |                                  --
      --| (__|  _/ |_| |                                  --
      -- \___|_|_ \___/ _____ ___  ___  _    _    ___ ___ --
      -- / __/ _ \| \| |_   _| _ \/ _ \| |  | |  | __| _ \--
      --| (_| (_) | .` | | | |   / (_) | |__| |__| _||   /--
      -- \___\___/|_|\_| |_| |_|_\\___/|____|____|___|_|_\--
      ------------------------------------------------------
      ---------------------
      --Combinatoric nets--
      ---------------------
      -- Implemented with variables to keep everything in sync

      -- Set k1
      case instr_op is
        when  ASM_NOP   => k1 := m_NOP   ;
        when  ASM_HALT  => k1 := m_HALT  ;
        when  ASM_GLOB  => k1 := m_GLOB  ;
        when  ASM_JMP   => k1 := m_JMP   ;
        when  ASM_JNZ   => k1 := m_JNZ   ;
        when  ASM_JZ    => k1 := m_JZ    ;
        when  ASM_JEQ   => k1 := m_JEQ   ;
        when  ASM_JNE   => k1 := m_JNE   ;
        when  ASM_JSR   => k1 := m_JSR   ;
        when  ASM_RTS   => k1 := m_RTS   ;
        when  ASM_RTI   => k1 := m_RTI   ;
        when  ASM_SFLAG => k1 := m_SFLAG ;
        when  ASM_LFLAG => k1 := m_LFLAG ;
        when  ASM_PUSH  => k1 := m_PUSH  ;
        when  ASM_POP   => k1 := m_POP   ;
        when  ASM_SSP   => k1 := m_SSP   ;
        when  ASM_LSP   => k1 := m_LSP   ;
        when  ASM_LOAD  => k1 := m_LOAD  ;
        when  ASM_STORE => k1 := m_STORE ;
        when  ASM_ADD   => k1 := m_ADD   ;
        when  ASM_SUB   => k1 := m_SUB   ;
        when  ASM_MUL   => k1 := m_MUL   ;
        when  ASM_MULU  => k1 := m_MULU  ;
        when  ASM_LSL   => k1 := m_LSL   ;
        when  ASM_LSR   => k1 := m_LSR   ;
        when  ASM_ASR   => k1 := m_ASR   ;
        when  ASM_ASL   => k1 := m_ASL   ;
        when  ASM_LRR   => k1 := m_LRR   ;
        when  ASM_LRL   => k1 := m_LRL   ;
        when  ASM_AND   => k1 := m_AND   ;
        when  ASM_OR    => k1 := m_OR    ;
        when  ASM_NOT   => k1 := m_NOT   ;
        when  ASM_XOR   => k1 := m_XOR   ;
        when  ASM_CP    => k1 := m_CP    ;
        when  ASM_SWAP  => k1 := m_SWAP  ;
        when  ASM_CLR   => k1 := m_CLR   ;
        when others     => k1 := "000000000";
      end case;

      -- Set k2
      -- case instr_amod is
      --   when ASM_CONSTANT  => k2 := m_CONSTANT  ;
      --   when ASM_IMMEDIATE => k2 := m_IMMEDIATE ;
      --   when ASM_INDIRECT  => k2 := m_INDIRECT  ;
      --   when ASM_INDEXED   => k2 := m_INDEXED   ;
      --   when others        => k2 := "000000000";
      -- end case;
      k2 := "000000000";
      if instr_amod = ASM_CONSTANT then
        k2 := m_CONSTANT;
      else
        k2 := m_INDIRECT;
      end if;

      -----------------------------
      --CPU CONTROLLER flow logic--
      -----------------------------
      -- Calculate a new mpc
      case m_row.seq is
        when "0000" => mpc_next := mpc + 1;      -- uPC++
        when "0001" => mpc_next := k1;           -- uPC := k1
        when "0010" => mpc_next := k2;           -- uPC := k2
        when "0011" =>
          if interrupt.flag = '1' then
            mpc_next := m_intr;                  --jump to intr
            interrupt.flag <= '0';
          else
            mpc_next := "000000000";             -- uPC := 0
          end if;
        -- Jump when Z = 0
        when "0100" => if z = '0'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc+1;
                       end if;

        -- Unconditional jump
        when "0101" => mpc_next := m_row.adr;    -- jump to adr

        -- Subroutine jump
        when "0110" => mpc_next := m_row.adr;

        -- mpc_next := from subroutine
        when "0111" => mpc_next := mpc_stack;

        -- Jump when Z = 1
        when "1000" => if z = '1'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc + 1;
                       end if;

        -- Jump when N = 1
        when "1001" => if n = '1'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc + 1;
                       end if;

        -- Jump when C = 1
        when "1010" => if c = '1'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc + 1;
                       end if;

        -- Jump when O = 1
        when "1011" => if o = '1'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc + 1;
                       end if;

        -- Jump when L = 1
        when "1100" => if l = '1'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc + 1;
                       end if;

        -- Jump when C = 0
        when "1101" => if c = '0'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc + 1;
                       end if;

        -- Jump when O = 0
        when "1110" => if c = '0'
                       then mpc_next := m_row.adr;
                       else mpc_next := mpc + 1;
                       end if;
        -- Halt
        when others => mpc_next := mpc;
      end case;

      -- Handle the loop flag
      if loop_counter = "00000000"
      then l <= '1';
      else l <= '0';
      end if;

      -- Save the state if needed
      if m_row.seq = "0110" then mpc_stack <= mpc; end if;

      -- Update the mpc.
      mpc <= mpc_next;

      ---------------------------------
      --Get next row in micro program--
      ---------------------------------
      m_row.to_buss   <= m_mem (conv_integer(mpc_next)) (31 downto 28);
      m_row.from_buss <= m_mem (conv_integer(mpc_next)) (27 downto 24);
      m_row.alu       <= m_mem (conv_integer(mpc_next)) (23 downto 19);
      m_row.seq       <= m_mem (conv_integer(mpc_next)) (18 downto 15);
      m_row.p         <= m_mem (conv_integer(mpc_next)) (14 downto 13);
      m_row.lc        <= m_mem (conv_integer(mpc_next)) (12 downto 11);
      m_row.s         <= m_mem (conv_integer(mpc_next)) (10 downto 9);
      m_row.res       <= '0';
      m_row.adr       <= m_mem (conv_integer(mpc_next)) (8 downto 0);


      --------------------------------------
      --|\   ____\|\   __  \|\  \|\  \    --
      --\ \  \___|\ \  \|\  \ \  \\\  \   --
      -- \ \  \    \ \   ____\ \  \\\  \  --
      --  \ \  \____\ \  \___|\ \  \\\  \ --
      --   \ \_______\ \__\    \ \_______\--
      --    \|_______|\|__|     \|_______|--
      --------------------------------------

      --------------------------------------------------------
      ----Transfer data according to microcode instruction----
      --------------------------------------------------------
      case m_row.to_buss is
        when BUS_IR  => buss := ir;
        when BUS_PM  => buss := cpu_read;
        when BUS_PC  => buss := pc;
        when BUS_SP  => buss := sp;
        when BUS_AR  => buss := ar;
        when BUS_HR  => buss := hr;
        when BUS_GRA => buss := gr(conv_integer(instr_gra));
        when BUS_GRB => buss := gr(conv_integer(instr_grb));
        when BUS_MCM => buss := X"00" & m_mem(conv_integer(mpc)) (7 downto 0);
        when BUS_ITR => buss := interrupt.data;
        when BUS_IADR => buss := intr_base + ("0" & interrupt.vec) + ("0" & interrupt.vec);
        when BUS_FLG =>
          buss(0) := n;
          buss(1) := z;
          buss(2) := o;
          buss(3) := c;
          buss(15 downto 4) := "000000000000";
        when others  => buss := X"0000";
      end case;

      case m_row.from_buss is
        when BUS_IR  => ir                            <= buss;
        when BUS_PM  => cpu_write                     <= buss;
        when BUS_ASR => asr                           <= buss;
        when BUS_SP  => sp                            <= buss;
        when BUS_PC  => pc                            <= buss;
        when BUS_HR  => hr                            <= buss;
        when BUS_GRA => gr(conv_integer(instr_gra))   <= buss;
        when BUS_GRB => gr(conv_integer(instr_grb))   <= buss;
        when BUS_GLO => glob(conv_integer(instr_gra)) <= buss;
        when BUS_FLG =>
          n <= buss(0);
          z <= buss(1);
          o <= buss(2);
          c <= buss(3);
        when others  => null;
      end case;

      temp  := gr(5);

      -- Set the memory write enable flag correctly
      case m_row.from_buss is
        when BUS_PM => cpu_enable_write <= '1';
        when others => cpu_enable_write <= '0';
      end case;

      -- What can the alu do for us?
      alu_d <= alu (alu_d, m_row.alu, buss);

      -- Increment the loop counter
      case m_row.lc is
        when "00" => loop_counter <= loop_counter;
        when "01" => if loop_counter /= X"00" then
                       loop_counter <= loop_counter - 1;
                     end if;
        when "10" => loop_counter <= buss (7 downto 0);
        when others => loop_counter <= m_row.adr (7 downto 0);
      end case;

      -- Update PC
      if m_row.from_buss /= BUS_PC then
        case m_row.p is
          when "00" => pc <= pc;
          when "01" => pc <= pc;
          when "10" => pc <= pc + 2;
          when  others => pc <= pc;
        end case;
      end if;

      -- Update SP
      if m_row.from_buss /= BUS_SP then
        case m_row.s is
          when "01"    => sp <= sp - 2;
          when "10"    => sp <= sp + 2;
          when  others => sp <= sp;
        end case;
      end if;
    end if;
  end process;
end Behavioral;
