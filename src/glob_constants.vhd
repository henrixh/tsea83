library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package glob_constants is
  --Constants that defines what every global register is
  constant glob_seven_seg   : integer := 0;
  constant glob_gpu_base    : integer := 1;
  constant glob_leds        : integer := 2;
  constant glob_intr_base   : integer := 3;
  constant glob_intr_enable : integer := 4;
  constant glob_sound4      : integer := 8;
  constant glob_sound1      : integer := 9;
  constant glob_sound2      : integer := 10;
  constant glob_sound3      : integer := 11;
  constant glob_timer1      : integer := 12;
  constant glob_timer2      : integer := 13;
  constant glob_timer3      : integer := 14;
  constant glob_timer4      : integer := 15;
end;

package body glob_constants is
end package body;
