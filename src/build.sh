#!/bin/sh

echo ""
echo "---------------- STARTING SYNTH ----------------------"
echo "Cleaning previous build..."
make clean > /dev/null

echo "Synting stuff..."
make proj.synth > synth.out || { echo "Synth failed with:"; \
                                 cat synth.out | grep -E 'ERROR|EXCEPTION|WARNING'; \
                                 exit 1; }

cat synth.out | grep -E 'ERROR|EXCEPTION|WARNING'; \

if [ "$HOSTNAME" = ixtab.edu.isy.liu.se ]; then
    echo "On ixtab. Skipping bitgen and programming."
else
    echo "Bitgen stuff..."
    make proj.bitgen > bitgen.out || { echo "Bitgen failed with"; \
                                   cat bitgen.out | grep 'ERROR|EXCEPTION|WARNING'; \
                                   exit 1; }

    cat bitgen.out | grep 'ERROR|EXCEPTION|WARNING'

    echo -n ''
    echo "Press ENTER to program FPGA"
    read -s

    echo "Program fpga..."
    make proj.prog > prog.out || { echo "Programming failed" ; exit 1; }
    echo "Programming successful!"
fi
