library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.random_access_memories.all;
use work.types.all;

entity memory is
port (
    -- Port A
    a_clk   : in  std_logic;
    a_wr    : in  std_logic;
    a_addr  : in  std_logic_vector(15 downto 0);
    a_din   : in  std_logic_vector(15 downto 0);
    a_dout  : out std_logic_vector(15 downto 0);

    -- Port B
    b_clk   : in  std_logic;
    b_wr    : in  std_logic;
    b_addr  : in  std_logic_vector(15 downto 0);
    b_din   : in  std_logic_vector(15 downto 0);
    b_dout  : out std_logic_vector(15 downto 0)
);
end memory;

architecture rtl of memory is
    -- Shared memory
    shared variable mem : word_array(0 to 32767) := primary_memory;
begin

-- Port A
process(a_clk)
begin
    if rising_edge(a_clk) then
        if(a_wr='1') then
            mem(conv_integer(a_addr (15 downto 1))) := a_din;
        end if;
        a_dout <= mem(conv_integer(a_addr (15 downto 1)));
    end if;
end process;

-- Port B
process(b_clk)
begin
    if rising_edge(b_clk) then
        if(b_wr='1') then
            mem(conv_integer(b_addr (15 downto 1))) := b_din;
        end if;
        b_dout <= mem(conv_integer(b_addr (15 downto 1)));
    end if;
end process;

end rtl;
