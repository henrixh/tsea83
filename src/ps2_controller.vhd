library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.types.ALL;


entity ps2_controller is
  port (clk, rst : in std_logic;
        ps2_clk : in std_logic;
        ps2_data : in std_logic;
        data : out word;
        interrupt : out std_logic
        );
end ps2_controller;


architecture Behavioral of ps2_controller is
  type status is (reading, waiting);
  signal state : status := waiting;
  signal prev_clk, sync_clk, sync_data : std_logic;
  signal num_bits : std_logic_vector(3 downto 0) := X"0";
  signal shift_register : std_logic_vector(10 downto 0) := "00000000000";
  signal key_up, extended: std_logic := '0';

  -- Function that counts number of ones in a vector
  function count_ones(byte : std_logic_vector) return std_logic_vector is
    variable result : std_logic_vector(3 downto 0) := X"0";
  begin
    for i in byte'range loop
      if byte(i) = '1' then result := result + 1;
      end if;
    end loop;
    return result;
  end function count_ones;

begin

  process(clk)
  begin
    if (rising_edge(clk)) then
      interrupt <= '0';
      data <= X"0000";

      if(rst = '1') then

        ---------
        --Reset--
        ---------
        num_bits <= X"0";
        interrupt <= '0';
        data <= X"0000";
        state <= waiting;
        shift_register <= "00000000000";
      else

        -- Synchronise ps2 signals
        sync_clk <= ps2_clk;
        sync_data <= ps2_data;

        -- Save previous clk value
        prev_clk <= sync_clk;

        -------------
        --Read Data--
        -------------
        if prev_clk = '1' and sync_clk = '0'  then
          if state = waiting and ps2_data <= '0' then  -- Start bit
            state <= reading;
            num_bits <= num_bits + 1;
          elsif state <= reading then  --Data bit
            shift_register(conv_integer (num_bits)) <= sync_data;
            num_bits <= num_bits + 1;
          end if;
        end if;

        ----------------
        --Done reading--
        ----------------
        if num_bits = X"A" then
          --Reset signals
          num_bits <= X"0";
          shift_register <= "00000000000";
          state <= waiting;

          --Check parity bit
          if count_ones(shift_register(8 downto 1))(0) /= shift_register(9) then
            --Set extended and key up flags
            if shift_register(8 downto 1) = X"E0" then
              extended <= '1';
            elsif shift_register(8 downto 1) = X"F0" then
              key_up <= '1';
            else
              -- Setup data and send interrupt
              interrupt <= '1';
              data(7 downto 0) <= shift_register(8 downto 1);
              data(15) <= key_up;
              data(14) <= extended;
              data(13 downto 8) <= "000000";
              extended <= '0';
              key_up <= '0';
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;
end Behavioral;
