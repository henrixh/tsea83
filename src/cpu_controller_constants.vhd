library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package cpu_controller_constants is
-- Define the op-codes for all assembly commands
  constant ASM_NOP   : std_logic_vector (5 downto 0) := "000000";
  constant ASM_HALT  : std_logic_vector (5 downto 0) := "000001";
  constant ASM_GLOB  : std_logic_vector (5 downto 0) := "000010";
  constant ASM_JMP   : std_logic_vector (5 downto 0) := "000011";
  constant ASM_JNZ   : std_logic_vector (5 downto 0) := "000100";
  constant ASM_JZ    : std_logic_vector (5 downto 0) := "000101";
  constant ASM_JEQ   : std_logic_vector (5 downto 0) := "000110";
  constant ASM_JNE   : std_logic_vector (5 downto 0) := "000111";
  constant ASM_JSR   : std_logic_vector (5 downto 0) := "001000";
  constant ASM_RTS   : std_logic_vector (5 downto 0) := "001001";
  constant ASM_RTI   : std_logic_vector (5 downto 0) := "001010";
  constant ASM_SFLAG : std_logic_vector (5 downto 0) := "001011";
  constant ASM_LFLAG : std_logic_vector (5 downto 0) := "001100";
  constant ASM_PUSH  : std_logic_vector (5 downto 0) := "001101";
  constant ASM_POP   : std_logic_vector (5 downto 0) := "001110";
  constant ASM_SSP   : std_logic_vector (5 downto 0) := "001111";
  constant ASM_LSP   : std_logic_vector (5 downto 0) := "010000";
  constant ASM_LOAD  : std_logic_vector (5 downto 0) := "010001";
  constant ASM_STORE : std_logic_vector (5 downto 0) := "010010";
  constant ASM_ADD   : std_logic_vector (5 downto 0) := "010011";
  constant ASM_SUB   : std_logic_vector (5 downto 0) := "010100";
  constant ASM_MUL   : std_logic_vector (5 downto 0) := "010101";
  constant ASM_MULU  : std_logic_vector (5 downto 0) := "010110";
  constant ASM_LSL   : std_logic_vector (5 downto 0) := "010111";
  constant ASM_LSR   : std_logic_vector (5 downto 0) := "011000";
  constant ASM_ASR   : std_logic_vector (5 downto 0) := "011001";
  constant ASM_ASL   : std_logic_vector (5 downto 0) := "011010";
  constant ASM_LRR   : std_logic_vector (5 downto 0) := "011011";
  constant ASM_LRL   : std_logic_vector (5 downto 0) := "011100";
  constant ASM_AND   : std_logic_vector (5 downto 0) := "011101";
  constant ASM_OR    : std_logic_vector (5 downto 0) := "011110";
  constant ASM_NOT   : std_logic_vector (5 downto 0) := "011111";
  constant ASM_XOR   : std_logic_vector (5 downto 0) := "100000";
  constant ASM_CP    : std_logic_vector (5 downto 0) := "100001";
  constant ASM_SWAP  : std_logic_vector (5 downto 0) := "100010";
  constant ASM_CLR   : std_logic_vector (5 downto 0) := "100011";

-- Define the "op-codes" for adressing modes
  constant ASM_CONSTANT  : std_logic_vector (1 downto 0) := "00";
  constant ASM_IMMEDIATE : std_logic_vector (1 downto 0) := "01";
  constant ASM_INDIRECT  : std_logic_vector (1 downto 0) := "10";
  constant ASM_INDEXED   : std_logic_vector (1 downto 0) := "11";

-- Define constanst for the alu.
  constant ALU_NOP    : std_logic_vector (4 downto 0) :=  "00000";
  constant ALU_SET    : std_logic_vector (4 downto 0) :=  "00001";
  constant ALU_NOT    : std_logic_vector (4 downto 0) :=  "00010";
  constant ALU_RESET  : std_logic_vector (4 downto 0) :=  "00011";
  constant ALU_ADD    : std_logic_vector (4 downto 0) :=  "00100";
  constant ALU_SUB    : std_logic_vector (4 downto 0) :=  "00101";
  constant ALU_MUL    : std_logic_vector (4 downto 0) :=  "00110";
  constant ALU_MULU   : std_logic_vector (4 downto 0) :=  "00111";
  constant ALU_AND    : std_logic_vector (4 downto 0) :=  "01000";
  constant ALU_OR     : std_logic_vector (4 downto 0) :=  "01001";
  constant ALU_XOR    : std_logic_vector (4 downto 0) :=  "01010";
  constant ALU_ADD_NF : std_logic_vector (4 downto 0) :=  "01011";
  constant ALU_LSL    : std_logic_vector (4 downto 0) :=  "01100";
  constant ALU_LSR    : std_logic_vector (4 downto 0) :=  "01101";
  constant ALU_ASL    : std_logic_vector (4 downto 0) :=  "01110";
  constant ALU_ASR    : std_logic_vector (4 downto 0) :=  "01111";
  constant ALU_LRL    : std_logic_vector (4 downto 0) :=  "10000";
  constant ALU_LRR    : std_logic_vector (4 downto 0) :=  "10001";

-- Define constants for bus identifiers
  constant BUS_PM  : std_logic_vector (3 downto 0) := "0001";
  constant BUS_ASR : std_logic_vector (3 downto 0) := "0010";
  constant BUS_PC  : std_logic_vector (3 downto 0) := "0011";
  constant BUS_SP  : std_logic_vector (3 downto 0) := "0100";
  constant BUS_AR  : std_logic_vector (3 downto 0) := "0101";
  constant BUS_HR  : std_logic_vector (3 downto 0) := "0110";
  constant BUS_GRA : std_logic_vector (3 downto 0) := "0111";
  constant BUS_GRB : std_logic_vector (3 downto 0) := "1000";
  constant BUS_MCM : std_logic_vector (3 downto 0) := "1001";
  constant BUS_GLO : std_logic_vector (3 downto 0) := "1010";
  constant BUS_IR  : std_logic_vector (3 downto 0) := "1011";
  constant BUS_ITR : std_logic_vector (3 downto 0) := "1100";
  constant BUS_IADR: std_logic_vector (3 downto 0) := "1101";
  constant BUS_FLG : std_logic_vector (3 downto 0) := "1110";
end;

package body cpu_controller_constants is

end package body;
