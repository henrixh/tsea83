library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.types.ALL;
use work.sound_constants.ALL;
--use work.random_access_memories.ALL;
--use work.cpu_controller_constants.ALL;
--use work.glob_constants.ALL;
--use work.madr_constants.ALL;
--use work.alu.ALL;

entity sound_card_rnd is
  port(clk, rst  : in STD_LOGIC;
       sound : out STD_LOGIC;
       control : word;
       random_data : word);
end sound_card_rnd;

architecture Behavioral of sound_card_rnd is
  signal timer       : STD_LOGIC_VECTOR (31 downto 0) := X"00000000";
  signal maxtimer    : STD_LOGIC_VECTOR (31 downto 0) := X"00000000";
  signal toggletimer : STD_LOGIC_VECTOR (31 downto 0) := X"00000000";

begin
  process (clk)
  begin
    if rising_edge (clk) then

      -- Set maxtimer
      case control (6 downto 0) is
        when TONE_OFF => maxtimer (24 downto 1) <= FREQ_OFF;
        when TONE_C0  => maxtimer (24 downto 1) <= FREQ_C0  ;
        when TONE_Ci0 => maxtimer (24 downto 1) <= FREQ_Ci0 ;
        when TONE_D0  => maxtimer (24 downto 1) <= FREQ_D0  ;
        when TONE_Di0 => maxtimer (24 downto 1) <= FREQ_Di0 ;
        when TONE_E0  => maxtimer (24 downto 1) <= FREQ_E0  ;
        when TONE_F0  => maxtimer (24 downto 1) <= FREQ_F0  ;
        when TONE_Fi0 => maxtimer (24 downto 1) <= FREQ_Fi0 ;
        when TONE_G0  => maxtimer (24 downto 1) <= FREQ_G0  ;
        when TONE_Gi0 => maxtimer (24 downto 1) <= FREQ_Gi0 ;
        when TONE_A0  => maxtimer (24 downto 1) <= FREQ_A0  ;
        when TONE_Ai0 => maxtimer (24 downto 1) <= FREQ_Ai0 ;
        when TONE_B0  => maxtimer (24 downto 1) <= FREQ_B0  ;
        when TONE_C1  => maxtimer (24 downto 1) <= FREQ_C1  ;
        when TONE_Ci1 => maxtimer (24 downto 1) <= FREQ_Ci1 ;
        when TONE_D1  => maxtimer (24 downto 1) <= FREQ_D1  ;
        when TONE_Di1 => maxtimer (24 downto 1) <= FREQ_Di1 ;
        when TONE_E1  => maxtimer (24 downto 1) <= FREQ_E1  ;
        when TONE_F1  => maxtimer (24 downto 1) <= FREQ_F1  ;
        when TONE_Fi1 => maxtimer (24 downto 1) <= FREQ_Fi1 ;
        when TONE_G1  => maxtimer (24 downto 1) <= FREQ_G1  ;
        when TONE_Gi1 => maxtimer (24 downto 1) <= FREQ_Gi1 ;
        when TONE_A1  => maxtimer (24 downto 1) <= FREQ_A1  ;
        when TONE_Ai1 => maxtimer (24 downto 1) <= FREQ_Ai1 ;
        when TONE_B1  => maxtimer (24 downto 1) <= FREQ_B1  ;
        when TONE_C2  => maxtimer (24 downto 1) <= FREQ_C2  ;
        when TONE_Ci2 => maxtimer (24 downto 1) <= FREQ_Ci2 ;
        when TONE_D2  => maxtimer (24 downto 1) <= FREQ_D2  ;
        when TONE_Di2 => maxtimer (24 downto 1) <= FREQ_Di2 ;
        when TONE_E2  => maxtimer (24 downto 1) <= FREQ_E2  ;
        when TONE_F2  => maxtimer (24 downto 1) <= FREQ_F2  ;
        when TONE_Fi2 => maxtimer (24 downto 1) <= FREQ_Fi2 ;
        when TONE_G2  => maxtimer (24 downto 1) <= FREQ_G2  ;
        when TONE_Gi2 => maxtimer (24 downto 1) <= FREQ_Gi2 ;
        when TONE_A2  => maxtimer (24 downto 1) <= FREQ_A2  ;
        when TONE_Ai2 => maxtimer (24 downto 1) <= FREQ_Ai2 ;
        when TONE_B2  => maxtimer (24 downto 1) <= FREQ_B2  ;
        when TONE_C3  => maxtimer (24 downto 1) <= FREQ_C3  ;
        when TONE_Ci3 => maxtimer (24 downto 1) <= FREQ_Ci3 ;
        when TONE_D3  => maxtimer (24 downto 1) <= FREQ_D3  ;
        when TONE_Di3 => maxtimer (24 downto 1) <= FREQ_Di3 ;
        when TONE_E3  => maxtimer (24 downto 1) <= FREQ_E3  ;
        when TONE_F3  => maxtimer (24 downto 1) <= FREQ_F3  ;
        when TONE_Fi3 => maxtimer (24 downto 1) <= FREQ_Fi3 ;
        when TONE_G3  => maxtimer (24 downto 1) <= FREQ_G3  ;
        when TONE_Gi3 => maxtimer (24 downto 1) <= FREQ_Gi3 ;
        when TONE_A3  => maxtimer (24 downto 1) <= FREQ_A3  ;
        when TONE_Ai3 => maxtimer (24 downto 1) <= FREQ_Ai3 ;
        when TONE_B3  => maxtimer (24 downto 1) <= FREQ_B3  ;
        when TONE_C4  => maxtimer (24 downto 1) <= FREQ_C4  ;
        when TONE_Ci4 => maxtimer (24 downto 1) <= FREQ_Ci4 ;
        when TONE_D4  => maxtimer (24 downto 1) <= FREQ_D4  ;
        when TONE_Di4 => maxtimer (24 downto 1) <= FREQ_Di4 ;
        when TONE_E4  => maxtimer (24 downto 1) <= FREQ_E4  ;
        when TONE_F4  => maxtimer (24 downto 1) <= FREQ_F4  ;
        when TONE_Fi4 => maxtimer (24 downto 1) <= FREQ_Fi4 ;
        when TONE_G4  => maxtimer (24 downto 1) <= FREQ_G4  ;
        when TONE_Gi4 => maxtimer (24 downto 1) <= FREQ_Gi4 ;
        when TONE_A4  => maxtimer (24 downto 1) <= FREQ_A4  ;
        when TONE_Ai4 => maxtimer (24 downto 1) <= FREQ_Ai4 ;
        when TONE_B4  => maxtimer (24 downto 1) <= FREQ_B4  ;
        when TONE_C5  => maxtimer (24 downto 1) <= FREQ_C5  ;
        when TONE_Ci5 => maxtimer (24 downto 1) <= FREQ_Ci5 ;
        when TONE_D5  => maxtimer (24 downto 1) <= FREQ_D5  ;
        when TONE_Di5 => maxtimer (24 downto 1) <= FREQ_Di5 ;
        when TONE_E5  => maxtimer (24 downto 1) <= FREQ_E5  ;
        when TONE_F5  => maxtimer (24 downto 1) <= FREQ_F5  ;
        when TONE_Fi5 => maxtimer (24 downto 1) <= FREQ_Fi5 ;
        when TONE_G5  => maxtimer (24 downto 1) <= FREQ_G5  ;
        when TONE_Gi5 => maxtimer (24 downto 1) <= FREQ_Gi5 ;
        when TONE_A5  => maxtimer (24 downto 1) <= FREQ_A5  ;
        when TONE_Ai5 => maxtimer (24 downto 1) <= FREQ_Ai5 ;
        when TONE_B5  => maxtimer (24 downto 1) <= FREQ_B5  ;
        when TONE_C6  => maxtimer (24 downto 1) <= FREQ_C6  ;
        when TONE_Ci6 => maxtimer (24 downto 1) <= FREQ_Ci6 ;
        when TONE_D6  => maxtimer (24 downto 1) <= FREQ_D6  ;
        when TONE_Di6 => maxtimer (24 downto 1) <= FREQ_Di6 ;
        when TONE_E6  => maxtimer (24 downto 1) <= FREQ_E6  ;
        when TONE_F6  => maxtimer (24 downto 1) <= FREQ_F6  ;
        when TONE_Fi6 => maxtimer (24 downto 1) <= FREQ_Fi6 ;
        when TONE_G6  => maxtimer (24 downto 1) <= FREQ_G6  ;
        when TONE_Gi6 => maxtimer (24 downto 1) <= FREQ_Gi6 ;
        when TONE_A6  => maxtimer (24 downto 1) <= FREQ_A6  ;
        when TONE_Ai6 => maxtimer (24 downto 1) <= FREQ_Ai6 ;
        when TONE_B6  => maxtimer (24 downto 1) <= FREQ_B6  ;
        when TONE_C7  => maxtimer (24 downto 1) <= FREQ_C7  ;
        when TONE_Ci7 => maxtimer (24 downto 1) <= FREQ_Ci7 ;
        when TONE_D7  => maxtimer (24 downto 1) <= FREQ_D7  ;
        when TONE_Di7 => maxtimer (24 downto 1) <= FREQ_Di7 ;
        when TONE_E7  => maxtimer (24 downto 1) <= FREQ_E7  ;
        when TONE_F7  => maxtimer (24 downto 1) <= FREQ_F7  ;
        when TONE_Fi7 => maxtimer (24 downto 1) <= FREQ_Fi7 ;
        when TONE_G7  => maxtimer (24 downto 1) <= FREQ_G7  ;
        when TONE_Gi7 => maxtimer (24 downto 1) <= FREQ_Gi7 ;
        when TONE_A7  => maxtimer (24 downto 1) <= FREQ_A7  ;
        when TONE_Ai7 => maxtimer (24 downto 1) <= FREQ_Ai7 ;
        when TONE_B7  => maxtimer (24 downto 1) <= FREQ_B7  ;
        when TONE_C8  => maxtimer (24 downto 1) <= FREQ_C8  ;
        when TONE_Ci8 => maxtimer (24 downto 1) <= FREQ_Ci8 ;
        when TONE_D8  => maxtimer (24 downto 1) <= FREQ_D8  ;
        when TONE_Di8 => maxtimer (24 downto 1) <= FREQ_Di8 ;
        when TONE_E8  => maxtimer (24 downto 1) <= FREQ_E8  ;
        when TONE_F8  => maxtimer (24 downto 1) <= FREQ_F8  ;
        when TONE_Fi8 => maxtimer (24 downto 1) <= FREQ_Fi8 ;
        when TONE_G8  => maxtimer (24 downto 1) <= FREQ_G8  ;
        when TONE_Gi8 => maxtimer (24 downto 1) <= FREQ_Gi8 ;
        when TONE_A8  => maxtimer (24 downto 1) <= FREQ_A8  ;
        when TONE_Ai8 => maxtimer (24 downto 1) <= FREQ_Ai8 ;
        when TONE_B8  => maxtimer (24 downto 1) <= FREQ_B8  ;
        when others   => maxtimer (24 downto 1) <= FREQ_OFF ;
      end case;
      -- Maxtimer is now doubled
      maxtimer (31 downto 25) <= "0000000";
      maxtimer (0) <= '0';

      --1/2 of maxtimer
      toggletimer (30 downto 0) <= maxtimer (31 downto 1);
      toggletimer (31) <= '0';

      if maxtimer = X"00000000" then
        sound <= random_data (0);
      elsif timer = toggletimer then
        sound <= random_data (0);
        timer <= timer + 1;
      elsif timer >= maxtimer then
        timer <= X"00000000";
        sound <= random_data (0);
      else
        timer <= timer + 1;
      end if;

    end if;
  end process;
end Behavioral;
