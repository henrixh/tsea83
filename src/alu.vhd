library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.types.ALL;
use work.cpu_controller_constants.ALL;

package alu is
  function alu (alu_d :alu_t; ctl :std_logic_vector(4 downto 0); buss :word)
    return alu_t;
end;

-- The projects simple ALU.

package body alu is
  -- The ALU is implemented as a function in order to split it out nicely.
  function alu(alu_d :alu_t;                      -- Previous state of the alu
               ctl :std_logic_vector(4 downto 0); -- Control signal
               buss : word)                       -- The argument to operate on.
    return alu_t is
    variable result :  std_logic_vector(16 downto 0);
    variable return_value : alu_t;
  begin

    -- Perform arithemetic
    case ctl is
      when ALU_NOP    => result := '0' & alu_d.ar;
      when ALU_SET    => result := '0' & buss;
      when ALU_NOT    => result := '0' & not buss;
      when ALU_RESET  => result := '0' & X"0000";
      when ALU_ADD    => result := '0' & alu_d.ar + buss;
      when ALU_SUB    => result := '0' & alu_d.ar - buss;
      when ALU_MUL    => result := '0' & alu_d.ar(7 downto 0) * buss(7 downto 0);
      when ALU_MULU   => result :=  '0' & alu_d.ar(7 downto 0) * buss(7 downto 0);
      when ALU_AND    => result := '0' &  alu_d.ar and buss;
      when ALU_OR     => result := '0' &  alu_d.ar or buss;
      when ALU_XOR    => result := '0' &  alu_d.ar xor buss;
      when ALU_ADD_NF => result := '0' &  alu_d.ar + buss;
      when ALU_LSL    => result := '0' &  alu_d.ar(14 downto 0) & '0';
      when ALU_LSR    => result := '0' & '0' & alu_d.ar(15 downto 1) ;
      when ALU_ASL    => result := '0' &  alu_d.ar(14 downto 0) & '0';
      when ALU_ASR    => result := '0' &  alu_d.ar(15) & alu_d.ar(15 downto 1);
      when ALU_LRR    => result := '0' &  alu_d.ar(14 downto 0) & alu_d.ar(15);
      when others     => result := '0' &  alu_d.ar;
    end case;

    return_value.ar := result(15 downto 0);

    --Check flags, O and C flags are not implemented yet
    if (ctl = ALU_ADD_NF) then
      return_value.n := alu_d.n;
      return_value.z := alu_d.z;
      return_value.o := alu_d.o;
      return_value.c := alu_d.c;
    else
      if (result = X"0000") then
        return_value.z := '1';
      else
        return_value.z := '0';
      end if;
      return_value.n := return_value.ar(15);

    end if;

    return return_value;
  end function;

end package body;
