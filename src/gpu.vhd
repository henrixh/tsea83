library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity gpu is
  Port ( clk,rst : in  STD_LOGIC;
         Hsync : out STD_LOGIC;
         Vsync : out STD_LOGIC;
         vga : out  STD_LOGIC_VECTOR (7 downto 0);
         gpu_read : in STD_LOGIC_VECTOR (15 downto 0);
         addr_gpu : out STD_LOGIC_VECTOR (15 downto 0);
         gpu_base : in STD_LOGIC_VECTOR (15 downto 0));
end gpu;

architecture Behavioral of gpu is
  signal x_counter : std_logic_vector (11 downto 0) := X"000";
  signal y_counter : std_logic_vector (11 downto 0) := X"000";
  signal ce        : std_logic_vector (1 downto 0) := "00";

  constant screen_width  : std_logic_vector (11 downto 0) := X"280";
  constant h_sync_begin  : std_logic_vector (11 downto 0) := X"290";
  constant h_sync_end    : std_logic_vector (11 downto 0) := X"2F0";
  constant row_end       : std_logic_vector (11 downto 0) := X"320";

  constant screen_heigth : std_logic_vector (11 downto 0) := X"1E0";
  constant v_sync_begin  : std_logic_vector (11 downto 0) := X"1EA";
  constant v_sync_end    : std_logic_vector (11 downto 0) := X"1EC";
  constant screen_end    : std_logic_vector (11 downto 0) := X"209";


  alias x_big is x_counter (9 downto 2);
  alias y_big is y_counter (9 downto 2);
begin

  -------------------
  --Update counters--
  -------------------
  process(clk)
  begin
    if rising_edge(clk) then

      case ce is
        when "00" =>
          --Update x counter
          case x_counter is
            when row_end => x_counter <= X"000";
            when others => x_counter <= x_counter + 1;
          end case;

          --Update y counter
          case y_counter is
            when screen_end => if x_counter = row_end
                               then y_counter <= X"000";
                               else y_counter <= y_counter;
                               end if;
            when others => if x_counter = row_end
                           then y_counter <= y_counter + 1;
                           else y_counter <= y_counter;
                           end if;
          end case;

        when others => null;
      end case;

      ce <= ce + 1;
    end if;
  end process;

  ----------------------------
  --Handle H-sync and V-sync--
  ----------------------------
  process(clk)
  begin
    if rising_edge(clk) then

      --Set H-sync
      case x_counter is
        when h_sync_begin => Hsync <= '0';
        when h_sync_end   => Hsync <= '1';
        when others       => null;
      end case;

      --Set V-sync
      case y_counter is
        when v_sync_begin => Vsync <= '0';
        when v_sync_end   => Vsync <= '1';
        when others       => null;
      end case;

      --If we are on the screen
      if x_counter < screen_width and y_counter < screen_heigth
      then
        case rst is
          -- Test signal
          when '1' =>
            vga (2 downto 0)  <= y_counter (2 downto 0);
            vga (7 downto 2)  <= x_counter (7 downto 2);

          -- Output signal from memory
          when others =>
            addr_gpu <= gpu_base + (X"00" & x_big) + (X"A0"* (y_big));

            -- Odd or even address?
            case gpu_base(0) xor x_counter(2) is
              when '0' => vga <= gpu_read (15 downto 8);
              when others => vga <= gpu_read (7 downto 0);
            end case;
        end case;
      else
        vga <= X"00"; -- Black otherwise
      end if;
    end if;
  end process;
end Behavioral;
