library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.types.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity timer is
  port ( clk, rst       : in std_logic;
         wait_time      : in word;
         interupt       : out std_logic
         );
end timer;


architecture Behavioral of timer is
  constant clocks_per_ms  : std_logic_vector(19 downto 0) := X"186A0";
begin

  process(clk)
    variable clock_counter  : std_logic_vector(19 downto 0);
    variable ms_counter     : std_logic_vector(15 downto 0);
    variable current_wait_time : word;

  begin
    if(rising_edge(clk)) then
      interupt <= '0';

      --Reset
      if rst = '1' then
        clock_counter := X"00000";
        ms_counter := X"0000";
      else

        ---------------------------------------
        --Reset counters if wait time changes--
        ---------------------------------------
        if current_wait_time /= wait_time then
          current_wait_time := wait_time;
          ms_counter := X"0000";
          clock_counter := X"00000";
        end if;


        if current_wait_time /= X"0000" then


          -------------------
          -- Send interrupt --
          -------------------
          if ms_counter >= current_wait_time then
            interupt <= '1';
            ms_counter := X"0000";
          end if;

          ---------------------
          -- Update counters --
          ---------------------
          case clock_counter is
            when clocks_per_ms =>
              ms_counter := ms_counter + 1;
              clock_counter := X"00000";
            when others =>
              clock_counter := clock_counter + 1;
          end case;

        end if;
      end if;
    end if;
  end process;
end Behavioral;
