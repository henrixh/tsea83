library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


package interupt_constants is
  --Constants for interrupt vector
  constant i_ps2      : std_logic_vector(2 downto 0) := "000";
  constant i_timer1   : std_logic_vector(2 downto 0) := "001";
  constant i_timer2   : std_logic_vector(2 downto 0) := "010";
  constant i_timer3   : std_logic_vector(2 downto 0) := "011";
  constant i_timer4   : std_logic_vector(2 downto 0) := "100";

end;

package body interupt_constants is
end package body;
