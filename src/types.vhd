library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package types is
  -- Define word as 16 bits
  subtype WORD is STD_LOGIC_VECTOR(15 downto 0);
  -- Helper type for word arrays
  type WORD_ARRAY is array(natural range <>) of STD_LOGIC_VECTOR(15 downto 0);

  subtype byte is STD_LOGIC_VECTOR(7 downto 0);

  --Assembly row type
  type asm_t is record
    op : std_logic_vector(5 downto 0);
    amod : std_logic_vector(1 downto 0);
    gra : std_logic_vector(3 downto 0);
    grb : std_logic_vector(3 downto 0);
  end record;

  --Microcode row type
  TYPE m_row_t is record
    to_buss : STD_LOGIC_VECTOR(3 downto 0);
    from_buss : STD_LOGIC_VECTOR(3 downto 0);
    alu : STD_LOGIC_VECTOR(4 downto 0);
    seq : STD_LOGIC_VECTOR(3 downto 0);
    adr : STD_LOGIC_VECTOR(8 downto 0);
    lc : STD_LOGIC_VECTOR(1 downto 0);
    s : STD_LOGIC_VECTOR(1 downto 0);
    p : STD_LOGIC_VECTOR(1 downto 0);
    res : STD_LOGIC;
  end record;

  --Alu data type
  type alu_t is record
    n : std_logic;
    z : std_logic;
    o : std_logic;
    c : std_logic;
    ar : word;
  end record;

  --Interrupt data type
  type intr_t is record
    data : word;
    vec  : std_logic_vector(2 downto 0);
    flag : std_logic;
  end record;

  --Micro memory type
  type m_mem_t is array (0 to 511) of std_logic_vector (31 downto 0);

end;

package body types is

end package body;
