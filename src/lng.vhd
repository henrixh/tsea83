library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.types.ALL;

entity lng is
  port (clk, rst : in STD_LOGIC;
        random_data : out word);
end lng;

architecture Behavioral of lng is
  signal d : std_logic_vector (31 downto 0);
  constant c : std_logic_vector (31 downto 0) := X"00003039";
  constant a : std_logic_vector (31 downto 0) := X"41C64E6D";
begin
  process (clk)
    variable tmp : std_logic_vector (63 downto 0);
    begin
      if rising_edge(clk) then
        tmp := (a * d + (X"0000" & c));
        d <= tmp (31 downto 0);
        random_data <= d (17 downto 2);
      end if;
    end process;
end Behavioral;
  
