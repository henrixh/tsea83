library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.glob_constants.all;
use work.types.all;


entity interupt_handler is
  Port ( clk,rst       : in std_logic;
         intr_in       : in std_logic_vector(7 downto 0);
         intr_data_in  : in word_array(7 downto 0);
         intr_out      : out intr_t
         );
end interupt_handler;

architecture Behavioral of interupt_handler is

begin

  process(clk)
  begin
    if rising_edge(clk) then
      if rst = '0' then
        ----------------------------------
        --Looks for and setup interrupts--
        ----------------------------------
        if intr_in(0) = '1' then
          intr_out.data <= intr_data_in(0);
          intr_out.vec <= "000";
          intr_out.flag <= '1';
        elsif intr_in(1) = '1' then
          intr_out.data <= intr_data_in(1);
          intr_out.vec <= "001";
          intr_out.flag <= '1';
        elsif intr_in(2) = '1' then
          intr_out.data <= intr_data_in(2);
          intr_out.vec <= "010";
          intr_out.flag <= '1';
        elsif intr_in(3) = '1' then
          intr_out.data <= intr_data_in(3);
          intr_out.vec <= "011";
          intr_out.flag <= '1';
        elsif intr_in(4) = '1' then
          intr_out.data <= intr_data_in(4);
          intr_out.vec <= "100";
          intr_out.flag <= '1';
        elsif intr_in(5) = '1' then
          intr_out.data <= intr_data_in(5);
          intr_out.vec <= "101";
          intr_out.flag <= '1';
        elsif intr_in(6) = '1' then
          intr_out.data <= intr_data_in(6);
          intr_out.vec <= "110";
          intr_out.flag <= '1';
        elsif intr_in(7) = '1' then
          intr_out.data <= intr_data_in(7);
          intr_out.vec <= "111";
          intr_out.flag <= '1';
        else
          intr_out.flag <= '0';
        end if;
      end if;
    end if;
  end process;
end Behavioral;
