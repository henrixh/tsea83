library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.types.all;
use work.interupt_constants.all;
use work.glob_constants.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity main is
  Port ( clk,rst     : in  STD_LOGIC;
         seg         : out  STD_LOGIC_VECTOR(7 downto 0);
         an          : out  STD_LOGIC_VECTOR (3 downto 0);
         vga         : out STD_LOGIC_VECTOR (7 downto 0);
         sw          : in STD_LOGIC_VECTOR (7 downto 0);
         Hsync,Vsync : out STD_LOGIC;
         led         : out STD_LOGIC_VECTOR(7 downto 0);
         ps2_data_in : in std_logic;
         ps2_clk_in  : in std_logic;
         soundpin0   : out STD_LOGIC;
         soundpin1   : out STD_LOGIC;
         soundpin2   : out STD_LOGIC
         );
end main;

architecture Behavioral of main is
  --Memory signals
  signal gpu_read         : word;
  signal cpu_read         : word;
  signal cpu_write        : word;
  signal addr_gpu         : word;
  signal addr_cpu         : word;
  signal cpu_enable_write : STD_LOGIC;

  --Global registers
  signal glob             : word_array(15 downto 0) := (others => X"0000");

  --Interrupt signals
  signal intr_data        : word_array(7 downto 0) := (others => X"0000");
  signal interupts        : std_logic_vector(7 downto 0);
  signal intr             : intr_t;

  --RNG signals
  signal random_data      : word;

  --Sound signals
  signal sound0           : STD_LOGIC;
  signal sound1           : STD_LOGIC;
  signal sound2           : STD_LOGIC;
  signal sound3           : STD_LOGIC;

begin
  -- Seven segment display driver
  ledthing: entity work.leddriver port map (clk, rst, seg, an,
                                            glob(glob_seven_seg));
  -- GPU
  gputhing: entity work.gpu port map (clk, rst, hsync, vsync,
                                      vga,
                                      gpu_read,
                                      addr_gpu,
                                      glob(glob_gpu_base));
  -- Main memory
  memorything: entity work.memory port map (clk,
                                            cpu_enable_write,
                                            addr_cpu,
                                            cpu_write,
                                            cpu_read, clk,
                                            '0',
                                            addr_gpu,
                                            X"0000",
                                            gpu_read);
  -- CPU
  cputhing: entity work.cpu port map (clk, rst,
                                      cpu_read,
                                      intr,
                                      glob(glob_intr_base),
                                      glob(glob_intr_enable)(0),
                                      cpu_write,
                                      addr_cpu,
                                      cpu_enable_write,
                                      glob);
  -- Interrupt handler
  interupt_handler : entity work.interupt_handler port map(clk, rst,
                                                           interupts,
                                                           intr_data,
                                                           intr);
  -- PS2 Controller
  ps2thing: entity work.ps2_controller port map (clk, rst,
                                                 ps2_clk_in,
                                                 ps2_data_in,
                                                 intr_data(conv_integer(i_ps2)),
                                                 interupts(conv_integer(i_ps2)));
  -- Interrupt Timers
  timer1 : entity work.timer port map(clk, rst,
                                      glob(glob_timer1),
                                      interupts(conv_integer(i_timer1)));
  timer2 : entity work.timer port map(clk, rst,
                                      glob(glob_timer2),
                                      interupts(conv_integer(i_timer2)));
  timer3 : entity work.timer port map(clk, rst,
                                      glob(glob_timer3),
                                      interupts(conv_integer(i_timer3)));
  timer4 : entity work.timer port map(clk, rst,
                                      glob(glob_timer4),
                                      interupts(conv_integer(i_timer4)));
  -- Sound cards
  sound_card1 : entity work.sound_card port map(clk, rst,
                                                sound0, glob(glob_sound1), random_data);
  sound_card2 : entity work.sound_card port map(clk, rst,
                                                sound1, glob(glob_sound2), random_data);
  sound_card3 : entity work.sound_card port map(clk, rst,
                                                sound2, glob(glob_sound3), random_data);
  sound_card4 : entity work.sound_card port map(clk, rst,
                                                sound3, glob(glob_sound4), random_data);
  -- Random number generator
  randomthing : entity work.lng port map(clk, rst, random_data);

  led <= glob(glob_sound1)(7 downto 0)
         xor glob(glob_sound2)(7 downto 0)
         xor glob(glob_sound3)(7 downto 0);

  soundPin0 <= sound0 xor sound3;
  soundPin1 <= sound1 xor sound3;
  soundPin2 <= sound2 xor sound3;

end Behavioral;
