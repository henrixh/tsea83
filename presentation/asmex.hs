  load gr0 (0xFFFE) -- Setup stack pointer
  ssp gr0
  state <- createInitialGameState framebuffer

  load gr0 framebuffer -- Setup framebuffer
  glob gpuBase gr0

  setupBoard state -- Create the board
  setupInterupts state -- Start the interrupts

  load gr0 0x0000 -- Unpause
  store gr0 (paused state)

  loop <- label -- Loop for all eternity
  jmp loop

  framebuffer <- label -- Put the framebuffer last
