import random
alphabet = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "space",
    "dot",
    "comma",
    "colon",
    "semicolor",
    "exclamation",
    "question",
    "hashtag",
    "at",
    "caret",
    "asterisk",
    "plus",
    "minus",
    "slash",
    "backslash",
    "leftparan",
    "rightparan",
    "leftbracket",
    "rightbracket",
    "leftbrace",
    "rightbrace",
    "smaller",
    "bigger",
    "equal",
    "pipe",
    "underscore",
    "tilde",
    "quote",
    "singlequote",
    "lmbda",
    "pi",
    "smiley",
    "heart",
    "square",
    "circle",
    "rhomb",
    "leftarrow",
    "rightarrow",
    "uparrow",
    "downarrow",
    "leftfull",
    "rightfull",
    "upfull",
    "downfull",
]

font = "\
00000\
0   0\
0   0\
0   0\
00000\
\
 11  \
  1  \
  1  \
  1  \
  1  \
\
22222\
    2\
22222\
2    \
22222\
\
33333\
    3\
  333\
    3\
33333\
\
4   4\
4   4\
44444\
    4\
    4\
\
55555\
5    \
55555\
    5\
55555\
\
66666\
6    \
66666\
6   6\
66666\
\
77777\
    7\
    7\
    7\
    7\
\
88888\
8   8\
88888\
8   8\
88888\
\
99999\
9   9\
99999\
    9\
    9\
\
AAAAA\
A   A\
AAAAA\
A   A\
A   A\
\
BBBB \
B   B\
BBBBB\
B   B\
BBBB \
\
CCCCC\
C    \
C    \
C    \
CCCCC\
\
DDDD \
D   D\
D   D\
D   D\
DDDD \
\
EEEEE\
E    \
EEE  \
E    \
EEEEE\
\
FFFFF\
F    \
FFF  \
F    \
F    \
\
GGGGG\
G    \
G GGG\
G   G\
GGGGG\
\
H   H\
H   H\
HHHHH\
H   H\
H   H\
\
IIIII\
  I  \
  I  \
  I  \
IIIII\
\
    J\
    J\
    J\
J   J\
JJJJJ\
\
K   K\
K  K \
KKK  \
K  K \
K   K\
\
L    \
L    \
L    \
L    \
LLLLL\
\
M   M\
MM MM\
M M M\
M   M\
M   M\
\
N   N\
NN  N\
N N N\
N  NN\
N   N\
\
OOOOO\
O   O\
O   O\
O   O\
OOOOO\
\
PPPPP\
P   P\
PPPPP\
P    \
P    \
\
QQQQQ\
Q   Q\
Q   Q\
Q  QQ\
QQQQQ\
\
RRRR \
R   R\
RRRRR\
R  R \
R   R\
\
SSSSS\
S    \
SSSSS\
    S\
SSSSS\
\
TTTTT\
  T  \
  T  \
  T  \
  T  \
\
U   U\
U   U\
U   U\
U   U\
UUUUU\
\
V   V\
V   V\
V   V\
 V V \
  V  \
\
W   W\
W   W\
W W W\
WW WW\
W   W\
\
X   X\
 X X \
  X  \
 X X \
X   X\
\
Y   Y\
Y   Y\
YYYYY\
  Y  \
  Y  \
\
ZZZZZ\
   Z \
  Z  \
 Z   \
ZZZZZ\
\
     \
     \
     \
     \
     \
\
     \
     \
     \
     \
  .  \
\
     \
     \
     \
   x \
  x  \
\
     \
  x  \
     \
  x  \
     \
\
     \
  ;  \
     \
  ;  \
  ;  \
\
  !  \
  !  \
  !  \
     \
  !  \
\
?????\
    ?\
  ???\
     \
  ?  \
\
 # # \
#####\
 # # \
#####\
 # # \
\
@@@@@\
@   @\
@ @@@\
@ @ @\
@ @@@\
\
  ^  \
 ^ ^ \
     \
     \
     \
\
* * *\
 *** \
* * *\
     \
     \
\
  +  \
  +  \
+++++\
  +  \
  +  \
\
     \
     \
 --- \
     \
     \
\
    /\
   / \
  /  \
 /   \
/    \
\
x    \
 x   \
  x  \
   x \
    x\
\
   ( \
  (  \
  (  \
  (  \
   ( \
\
 )   \
  )  \
  )  \
  )  \
 )   \
\
  [[ \
  [  \
  [  \
  [  \
  [[ \
\
 ]]  \
  ]  \
  ]  \
  ]  \
 ]]  \
\
  {{ \
  {  \
 {{  \
  {  \
  {{ \
\
 }}  \
  }  \
  }} \
  }  \
 }}  \
\
   < \
  <  \
 <   \
  <  \
   < \
\
 >   \
  >  \
   > \
  >  \
 >   \
\
     \
=====\
     \
=====\
     \
\
  |  \
  |  \
  |  \
  |  \
  |  \
\
     \
     \
     \
     \
_____\
\
     \
~~~  \
~ ~ ~\
  ~~~\
     \
\
 x x \
 x x \
     \
     \
     \
\
  '  \
  '  \
     \
     \
     \
\
x    \
 x   \
  x  \
 x x \
x   x\
\
     \
xxxxx\
 x x \
 x x \
 x x \
\
 x x \
 x x \
     \
x   x\
 xxx \
\
 x x \
xxxxx\
xxxxx\
 xxx \
  x  \
\
xxxxx\
xxxxx\
xxxxx\
xxxxx\
xxxxx\
\
 xxx \
xxxxx\
xxxxx\
xxxxx\
 xxx \
\
  x  \
 xxx \
xxxxx\
 xxx \
  x  \
\
  x  \
 x   \
xxxxx\
 x   \
  x  \
\
  x  \
   x \
xxxxx\
   x \
  x  \
\
  x  \
 xxx \
x x x\
  x  \
  x  \
\
  x  \
  x  \
x x x\
 xxx \
  x  \
\
  x  \
 xx  \
xxx  \
 xx  \
  x  \
\
  x  \
  xx \
  xxx\
  xx \
  x  \
\
  x  \
 xxx \
xxxxx\
     \
     \
\
     \
     \
xxxxx\
 xxx \
  x  \
\
"


print '{-# LANGUAGE RecursiveDo #-}'
print '-- |Main font module'
print 'module Font where'
print 'import TAsm'
print 'import Data.Word'
print '-- |Font data'
print 'font :: TAsm ()'
print 'font = do'

width = 160
size = 5
where = 0
row = 0
flags = {}
for k in range(len(alphabet)):
    where = 0
    flags[row] = 'c_' + alphabet[k]
    s = ""
    for i in range(size+1):
        t = ""
        for j in range(size+1):
            if i == size or j == size: c = ' '
            else: c = font[size**2*k + size*i + j]
            if c == ' ':
                c = '0'
            else:
                c = 'F'
                print '                dataRow ' + str(where)
                row += 2
                where = 0
            where += 1
            t += 2*c
        where += width - size-1
        #print t
        s += t
    print '                dataRow ' + str(2**16-1)
    row += 2
    #while s:
    #    print '                dataRow 0x' + s[:4]
    #    s = s[4:]

space = max([len(e) for e in alphabet]) + 3
for pos in sorted(flags.keys()):
    print flags[pos] + ' '*(space-len(flags[pos])) + '= ' + str(pos) + ' '*(4-len(str(pos))) + ' :: Word16'

exit()
word = ['c_square', 'c_0', 'c_1', 'c_space', 'c_square', 'c_0', 'c_0', 'c_space',  'c_space', 'c_R', 'c_O', 'c_U', 'c_N', 'c_D', 'c_colon', 'c_0', 'c_3', 'c_space', 'c_space', 'c_square', 'c_0', 'c_5', 'c_space', 'c_square', 'c_0', 'c_2']
color = ['0707',    'ffff','ffff','ffff',    '3838',     'ffff','ffff','ffff',     'ffff',    'ffff','ffff','ffff','ffff','ffff','ffff',    'ffff','ffff','ffff',    'ffff',    'c0c0',     'ffff','ffff','ffff',    '3f3f',     'ffff','ffff']
startx = 160/2-3*len(word)
x = startx
y = 50
print '  load gr1 {}'.format(y)
#for char in sorted(flags.values())*3:
for j in range(len(word)):
    char = word[j]
    #k = [str(hex(random.randint(0,15)))[2:] for i in range(4)]
    k = color[j]
    print '  load color 0x{}'.format(''.join(k))
    print '  load gr0 {}'.format(x)
    print '  load gr2 (fontbase + fromIntegral {})'.format(char)
    print '  jsr dc'
    x += 6
    if x > 154:
        x = startx
        y += 6
        if y > 114:
            exit()
        print '  load gr1 {}'.format(y)

